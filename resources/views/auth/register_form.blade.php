<div class="sl-form-register__wrap">
    <div class="f-size-lg-24 f-size-sm-20 f-size-16 f-weight-300 l-height-100">Реестрацiя</div>
    <form method="POST" action="{{ route('register') }}" class="sl-form-register__form">
        @csrf
        <div class="f-size-md-16 f-size-14 f-weight-300 l-height-100">Даннi для входу</div>

        <div class="d-flex flex-wrap align-items-center">
            <label class="required">
                <input id="email" type="email" class="@error('email') is-invalid @enderror"
                       name="email" value="{{ old('email') }}" required autocomplete="email"
                       placeholder="Поштова скринька">

                @error('email')
                <span class="invalid-feedback" role="alert" style="display: inline">
                            <strong>{{ $message }}</strong>
                        </span>
                @enderror
            </label>
            <label class="sl-form-register__form-checkbox ml-sm-20">
                <input type="checkbox" name="emailIsPublic" id="emailIsPublic" {{ old('emailIsPublic') ? 'checked' : '' }}>
                <span></span>
                Вiдображати у профiлi
            </label>
        </div>

        <label class="required">
            <input id="password" type="password" class="@error('password') is-invalid @enderror"
                   name="password" required autocomplete="new-password" placeholder="Пароль">

            @error('password')
            <span class="invalid-feedback" role="alert" style="display: inline">
                        <strong>{{ $message }}</strong>
                    </span>
            @enderror
        </label>

        <div class="f-size-md-16 f-size-14 f-weight-300 l-height-100 mt-20">Iнформацiя про особу</div>

        <label class="required">
            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                   name="name" value="{{ old('name') }}" required autocomplete="name"
                   placeholder="Iм`я">

            @error('name')
            <span class="invalid-feedback" role="alert" style="display: inline">
                        <strong>{{ $message }}</strong>
                    </span>
            @enderror
        </label>

        <label>
            <input id="last-name" type="text" class="form-control @error('last-name') is-invalid @enderror"
                   name="last-name" value="{{ old('last-name') }}" autocomplete="last-name"
                   placeholder="Прiзвище">

            @error('last-name')
            <span class="invalid-feedback" role="alert" style="display: inline">
                        <strong>{{ $message }}</strong>
                    </span>
            @enderror
        </label>

        <div class="d-flex flex-wrap align-items-center mb-30">
            <label class="required">
                <input type="tel" id="phone" class="@error('phone') is-invalid @enderror"
                       name="phone" value="{{ old('phone') }}" required autocomplete="phone" placeholder="Номер телефону">

                @error('phone')
                <span class="invalid-feedback" role="alert" style="display: inline">
                            <strong>{{ $message }}</strong>
                        </span>
                @enderror
            </label>

            <label class="sl-form-register__form-checkbox ml-sm-20">
                <input type="checkbox" name="phoneIsPublic" id="phoneIsPublic" {{ old('phoneIsPublic') ? 'checked' : '' }}>
                <span></span>
                Вiдображати у профiлi
            </label>
        </div>
        <button type="submit">Зарееструвати</button>
    </form>
</div>