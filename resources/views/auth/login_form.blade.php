<div class="sl-form-login__wrap">
    <div class="f-size-lg-24 f-size-sm-20 f-size-16 f-weight-300 l-height-100">Вхiд в особистий кабiнет</div>
    <div class="d-flex flex-wrap flex-sm-row flex-column pt-30">
        <form method="POST" action="{{ route('login') }}" class="sl-form-login__form">
            @csrf

            <label>
                <input type="text" name="email"
                       class="@error('email') is-invalid @enderror"
                       value="{{ old('email') }}" placeholder="Поштова скринька"
                       required autocomplete="email" autofocus>

                @error('email')
                <span class="invalid-feedback" role="alert" style="display: inline">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </label>

            <label>
                <input type="password" name="password"
                       class="@error('password') is-invalid @enderror"
                       value="{{ old('password') }}" placeholder="Пароль"
                       required autocomplete="current-password">

                @error('password')
                <span class="invalid-feedback" role="alert" style="display: inline">
        <strong>{{ $message }}</strong>
    </span>
                @enderror
            </label>

            <a class="sl-form-login__form-remind" href="{{ route('password.request') }}">Нагадати пароль</a>

            <div class="mt-10">
                <label class="sl-form-login__form-remember">
                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                    <span></span>
                    Запам'ятати мене
                </label>
            </div>

            <div class="d-flex flex-wrap mt-30 align-items-center">
                <button type="submit">Увiйти</button>
                <span class="f-size-14 f-weight-300 brand-gray-3-color px-15">або</span>
                <a data-remodal-target="register" class="sl-form-login__form-link" href="{{ route('register') }}">Зареєструватися</a>
            </div>
        </form>
        <div class="sl-form-login__open-cosial mt-sm-0 mt-25">
            <div class="f-size-14 f-weight-300 brand-gray-3-color mb-15">Увійти як користувач</div>
            <a class="sl-form-login__open-cosial-fb" href="{{route('auth-facebook')}}"></a>
            <a class="sl-form-login__open-cosial-gl" href="{{route('auth-google')}}"></a>
        </div>
    </div>
</div>