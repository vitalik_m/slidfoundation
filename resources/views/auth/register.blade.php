@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card mt-4">
                {{--<div class="card-header">{{ __('Register') }}</div>--}}

                <div class="card-body">
                    @include('auth.register_form')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
