@extends('layouts.app')

@section('content')
    <div class="sl-container sl-office-page">
        <div class="container-fluid mb-35">
            <div class="sl-office-page-col rating-page">

                <h1 class="sl-title-line-red f-size-lg-36 f-size-md-32 f-size-28 l-height-100 f-weight-300 mb-30">Рєйтинг</h1>

                <div class="f-size-md-14 f-size-13 f-weight-300 l-height-140 clearfix page-content page-about">
                    <table class="table table-hover table-striped table-sm">
                        <thead>
                            <tr>
                                <th scope="col">Місце в рєйтингу</th>
                                <th scope="col">Кількість слідів</th>
                                <th scope="col">Країна</th>
                                <th scope="col">Фото</th>
                                <th scope="col">Ім'я</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $i => $user)
                            <tr>
                                <th scope="row">{{ $i+1 }}</th>
                                <td class=" tracks-count">{{ $user->tracks_count }}</td>
                                <td>
                                    @if($user->countryCod && file_exists(public_path('/images/24/'.$user->countryCod.'.png')))
                                        <img class="sl-flag" src="{{ asset('/images/24/'. $user->countryCod .'.png') }}" alt="">
                                    @endif
                                </td>
                                <td>
                                    <div class="sl-img">
                                        <img src="{{ asset($user->photo ? $user->photo : $user->defaultPhoto) }}" alt="">
                                    </div>
                                </td>
                                <td class="f-size-14 name">
                                    <a href="{{ route('user.profile', $user->id) }}">{{$user->name }}</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('inner_js')
@stop