@extends('layouts.admin')

@section('title', 'Редагування новини')
@section('h1_title', 'Редагування новини')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary container">
                <div class="row">
                    {!! Form::model($newArticle, ['route' => ['newArticle.update', $newArticle->id], 'method' => 'patch', 'enctype' => 'multipart/form-data']) !!}

                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            {!! Form::label('title', 'Назва:') !!}
                            {!! Form::text('title', null, ['class' => 'form-control']) !!}
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="form-group note-editor-page">
                            {!! Form::label('content', 'Зміст:') !!}
                            <textarea class="form-control text-editor" name="content" cols="50" rows="20" id="page-content">
                                {!! $newArticle->content !!}
                            </textarea>
                        </div>
                    </div>

                    <div class="form-group col-xs-12">
                        <input type="submit" value="Оновити новину" class="btn btn-primary">
                    </div>
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </div>

@endsection

@section('inner_js')
    <script>
        $('.text-editor').summernote({
            height: 500
            }
        );
    </script>
@stop