@extends('layouts.admin')

@section('title', 'Нова новина')
@section('h1_title', 'Нова новина')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary container">
                <div class="row">
                    {!! Form::model($newArticle, ['route' => ['newArticle.store'], 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}

                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            {!! Form::label('title', 'Назва:') !!}
                            {!! Form::text('title', null, ['class' => 'form-control', 'required']) !!}
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="form-group note-editor-page">
                            {!! Form::label('content', 'Зміст:') !!}
                            <textarea class="form-control text-editor" name="content" cols="50" rows="20" id="page-content">
                                {!! $newArticle->content !!}
                            </textarea>
                        </div>
                    </div>

                    <div class="form-group col-xs-12">
                        <input type="submit" value="Створити новину" class="btn btn-primary">
                    </div>
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </div>

@endsection

@section('inner_js')
    <script>
        $('.newArticle-editor').summernote({
                height: 500
            }
        );
    </script>
@stop