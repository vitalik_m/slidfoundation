@extends('layouts.admin')

@section('title', 'Новини')
@section('h1_title', 'Новини')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <a href="{{ route('newArticle.create') }}" class="btn btn-primary">Створити нову новину</a>
                </div>

                <div class="box-body">
                    <table id="tracks_table" class="table table-bordered table-hover dataTable" role="grid" 
                           aria-describedby="tracks_table_info">
                        <thead>
                        <tr role="row">
                            <th class="sorting_desc" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1" >Id Новини</th>

                            <th class="sorting" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">Назва</th>
                            <th class="sorting" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">Зміст</th>
                            <th class="sorting" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">Створено</th>
                            <th tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">
                                
                            </th>
                        </tr>
                        </thead>

                        <tbody>
                        @forelse($newArticles as $newArticle)
                            <tr role="row" class="odd">
                                <td class="sorting_1"><a href="{{ route('newArticle.edit', $newArticle->id) }}">{{ $newArticle->id }}</a></td>

                                <td><a href="{{ route('newArticle.edit', $newArticle->id) }}">{{ $newArticle->title }}</a></td>
                                <td><a href="{{ route('newArticle.edit', $newArticle->id) }}">{{ Str::limit($newArticle->content, 150) }}</a></td>
                                <td><a href="{{ route('newArticle.edit', $newArticle->id) }}">{{ $newArticle->created_at }}</a></td>
                                <td>
                                    <a href="{{ route('newArticle.edit', $newArticle->id) }}" class="btn btn-default btn-block  btn-xs">
                                        <i class="fa fa-fw fa-edit"></i>
                                    </a>

                                    {!! Form::model($newArticle, ['route' => ['newArticle.destroy', $newArticle->id], 'method' => 'delete', 'enctype' => 'multipart/form-data']) !!}
                                        <button type="submit" class="btn btn-default btn-block  btn-xs">
                                            <i class="fa fa-fw fa-trash"></i>
                                        </button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @empty

                        @endforelse
                        </tbody>

                        <tfoot>
                        <tr>
                            <th class="sorting_desc" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1" >Id Новини</th>

                            <th class="sorting" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">Назва</th>
                            <th class="sorting" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">Зміст</th>
                            <th class="sorting" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">Створено</th>
                            <th tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">

                            </th>
                        </tr>
                        </tfoot>
                    </table>

                </div>

            </div>
        </div>
    </div>

@endsection

@section('inner_js')
    <script>
        $(function () {
            $('#tracks_table').DataTable({
                // 'paging'      : true,
                // 'lengthChange': false,
                'searching'   : false,
                paging: false,
                // 'ordering'    : true,
                'info'        : false,
                // 'autoWidth'   : false,
                // 'order': [[ 9, 'desc' ]]
            });
        });
    </script>
@stop