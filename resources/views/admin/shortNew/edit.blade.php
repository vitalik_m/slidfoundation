@extends('layouts.admin')

@section('title', 'Редагування швидкої новини')
@section('h1_title', 'Редагування швидкої новини')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary container">
                <div class="row">
                    {!! Form::model($shortNew, ['route' => ['shortNew.update', $shortNew->id], 'method' => 'patch', 'enctype' => 'multipart/form-data']) !!}

                    <div class="col-xs-12">
                        <div class="form-group note-editor-page">
                            {!! Form::label('content', 'Зміст:') !!}
                            <textarea class="form-control text-editor" name="content" cols="50" rows="20" id="page-content">
                                {!! $shortNew->content !!}
                            </textarea>
                        </div>
                    </div>

                    <div class="form-group col-xs-12">
                        <input type="submit" value="Оновити новину" class="btn btn-primary">
                    </div>
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </div>

@endsection

@section('inner_js')
    <script>
        $('.text-editor').summernote({
            height: 500
            }
        );
    </script>
@stop