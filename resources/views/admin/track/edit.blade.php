@extends('layouts.admin')

@section('title', 'Сліди')
@section('h1_title', 'Сліди')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary container">
                <div class="row">
                    {!! Form::model($track, ['route' => ['track.update', $track->id], 'method' => 'patch', 'enctype' => 'multipart/form-data']) !!}

                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            {!! Form::label('lon', 'Lon *:') !!}
                            {!! Form::text('lon', null, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            {!! Form::label('lat', 'Lat *:') !!}
                            {!! Form::text('lat', null, ['class' => 'form-control']) !!}
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            {!! Form::label('owner[name]', 'Власник *:') !!}
                            {!! Form::text('owner[name]', null, ['class' => 'form-control', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            {!! Form::label('status', 'Статус *:') !!}

                            <select class="form-control" id="status" name="status">
                                <option value="checked" {{ $track->status == 'checked' ? 'selected' : '' }}>checked</option>
                                <option value="pending" {{ $track->status == 'pending' ? 'selected' : '' }}>pending</option>
                                <option value="deny" {{ $track->status == 'deny' ? 'selected' : '' }}>deny</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6">

                        @if($track->video)
                            <div class="">
                                <iframe width="100%" height="auto" src="{{ $track->video }}"
                                        frameborder="0"
                                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                        allowfullscreen></iframe>
                            </div>
                        @endif

                        <div class="form-group">
                            {!! Form::label('video', 'Відео :') !!}
                            {!! Form::text('video', null, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        @if($track->img)
                            <div class="">
                                <img src="/{{ $track->img}}" class="img-responsive" id="preview-img">
                            </div>
                        @endif
                        <div class="form-group">
                            {!! Form::label('img', 'Зображення :') !!}
                            {!! Form::file('img', ['class' => 'form-control image-upload']) !!}
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            {!! Form::label('amount', 'Ціна:') !!}
                            {!! Form::text('amount', null, ['class' => 'form-control']) !!}
                        </div>
                    </div>

                    <div class="col-xs-12">

                        <div class="form-group">
                            {!! Form::label('title', 'Назва:') !!}
                            {!! Form::text('title', null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group note-editor-page">
                            {!! Form::label('content', 'Зміст:') !!}
                            <textarea class="form-control text-editor" name="content" cols="50" rows="10" id="value-text">
                                {!!  $track->content !!}
                            </textarea>
                        </div>
                    </div>

                    <div class="form-group col-xs-12">
                        <input type="submit" value="Оновити слід" class="btn btn-primary">
                    </div>
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </div>

@endsection

@section('inner_js')
    <script>
        $('.text-editor').summernote({
            height: 250
            }
        );
    </script>
@stop