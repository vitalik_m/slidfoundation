@extends('layouts.admin')

@section('title', 'Сліди')
@section('h1_title', 'Сліди')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    {{--<a href="{{ route('track.create') }}" class="btn btn-primary">Create</a>--}}
                    <form role="form" action="{{ route('track.bulkStore') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="exampleInputFile">Виберіть файл слідів</label>
                            <input type="file" name="tracksfile" id="exampleInputFile">
                        </div>

                        <button type="submit" class="btn btn-primary">Завантажити</button>
                    </form>
                </div>

                <div class="box-body">
                    <table id="tracks_table" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="tracks_table_info">
                        <thead>
                            <tr role="row">
                                <th class="sorting_desc" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1" >Id Сліда</th>

                                <th class="sorting" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">Назва</th>
                                <th class="sorting" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">Власник</th>
                                <th class="sorting" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">lat</th>
                                <th class="sorting" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">lon</th>
                                <th class="" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">Статус</th>
                                <th tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1"></th>
                            </tr>
                        </thead>

                        <tbody>
                        @forelse($tracks as $track)
                            <tr role="row" class="odd {{ $track->isCurrent ? 'current-track' : '' }}">
                                <td class="sorting_1"><a href="{{ route('track.edit', $track->id) }}">{{ $track->id }}</a></td>

                                <td><a href="{{ route('track.edit', $track->id) }}">{{ $track->title }}</a></td>
                                <td><a href="{{ route('track.edit', $track->id) }}">{{ $track->owner->name }}</a></td>
                                <td><a href="{{ route('track.edit', $track->id) }}">{{ $track->lat }}</a></td>
                                <td><a href="{{ route('track.edit', $track->id) }}">{{ $track->lon }}</a></td>

                                <td>
                                    @if($track->status == 'checked')
                                        <span class="label label-default">{{ $track->status }}</span>
                                    @elseif($track->status == 'deny')
                                        <span class="label label-warning">{{ $track->status }}</span>
                                    @elseif($track->status == 'pending')
                                        <span class="label label-info">{{ $track->status }}</span>
                                    @else
                                        <span class="label label-danger">{{ $track->status }}</span>
                                    @endif
                                </td>

                                <td>
                                    @if($track->isCurrent == 0)
                                    {{ Form::open(array('url' => route('track.setCurrent'), 'method' => 'post', 'class'=>'col-md-12')) }}
                                        {!! Form::hidden('trackId', $track->id) !!}
                                        {!! Form::button('Я тут!', [ 'type'=>'submit', 'class' => 'btn btn-primary']) !!}
                                    {{ Form::close() }}
                                    @endif
                                </td>
                            </tr>
                        @empty

                        @endforelse
                        </tbody>

                        <tfoot>
                            <tr>
                                <th class="sorting_desc" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1" >Id Сліда</th>

                                <th class="sorting" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">Назва</th>
                                <th class="sorting" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">Власник</th>
                                <th class="sorting" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">lat</th>
                                <th class="sorting" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">lon</th>
                                <th class="" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">Статус</th>
                                <th class="" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1"></th>
                            </tr>
                        </tfoot>
                    </table>

                    {{ $tracks->render() }}
                </div>

            </div>
        </div>
    </div>

@endsection

@section('inner_js')
    <script>
        $(function () {
            $('#tracks_table').DataTable({
                // 'paging'      : true,
                // 'lengthChange': false,
                'searching'   : false,
                paging: false,
                // 'ordering'    : true,
                'info'        : false,
                // 'autoWidth'   : false,
                // 'order': [[ 9, 'desc' ]]
            });
        });
    </script>
@stop