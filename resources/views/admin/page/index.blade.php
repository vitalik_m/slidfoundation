@extends('layouts.admin')

@section('title', 'Сторінки')
@section('h1_title', 'Сторінки')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                </div>

                <div class="box-body">
                    <table id="tracks_table" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="tracks_table_info">
                        <thead>
                            <tr role="row">
                                <th class="sorting_desc" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1" >Id Сторінки</th>

                                <th class="sorting" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">Опис</th>
                                <th class="sorting" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">Заголовок</th>
                                <th class="sorting" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">slug</th>
                                <th tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1"></th>
                            </tr>
                        </thead>

                        <tbody>
                        @forelse($pages as $page)
                            <tr role="row" class="odd">
                                <td class="sorting_1"><a href="{{ route('page.edit', $page->id) }}">{{ $page->id }}</a></td>

                                <td><a href="{{ route('page.edit', $page->id) }}">{{ $page->desc }}</a></td>
                                <td><a href="{{ route('page.edit', $page->id) }}">{{ $page->title }}</a></td>
                                <td><a href="{{ route('page.edit', $page->id) }}">{{ $page->slug }}</a></td>
                            </tr>
                        @empty

                        @endforelse
                        </tbody>

                        <tfoot>
                            <tr>
                                <th class="sorting_desc" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1" >Id Сторінки</th>

                                <th class="sorting" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">Опис</th>
                                <th class="sorting" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">Заголовок</th>
                                <th class="sorting" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">slug</th>
                                <th tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1"></th>
                            </tr>
                        </tfoot>
                    </table>

                </div>

            </div>
        </div>
    </div>

@endsection

@section('inner_js')
    <script>
        $(function () {
            $('#tracks_table').DataTable({
                // 'paging'      : true,
                // 'lengthChange': false,
                'searching'   : false,
                paging: false,
                // 'ordering'    : true,
                'info'        : false,
                // 'autoWidth'   : false,
                // 'order': [[ 9, 'desc' ]]
            });
        });
    </script>
@stop