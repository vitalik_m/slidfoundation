@extends('layouts.admin')

@section('title', 'Сліди')
@section('h1_title', 'Сліди')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary container">
                <div class="row">
                    {!! Form::model($page, ['route' => ['page.update', $page->id], 'method' => 'patch', 'enctype' => 'multipart/form-data']) !!}

                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            {!! Form::label('slug', 'Slug:') !!}
                            {!! Form::text('slug', null, ['class' => 'form-control', 'readonly']) !!}
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            {!! Form::label('desc', 'Опис:') !!}
                            {!! Form::text('desc', null, ['class' => 'form-control']) !!}
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            {!! Form::label('title', 'Заголовок:') !!}
                            {!! Form::text('title', null, ['class' => 'form-control']) !!}
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="form-group note-editor-page">
                            {!! Form::label('content', 'Зміст:') !!}
                            <textarea class="form-control text-editor" name="content" cols="50" rows="20" id="page-content">
                                {!!  $page->content !!}
                            </textarea>
                        </div>
                    </div>

                    <div class="form-group col-xs-12">
                        <input type="submit" value="Оновити сторінку" class="btn btn-primary">
                    </div>
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </div>

@endsection

@section('inner_js')
    <script>
        $('.text-editor').summernote({
            height: 450
            }
        );
    </script>
@stop