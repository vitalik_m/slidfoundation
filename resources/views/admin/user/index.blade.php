@extends('layouts.admin')

@section('title', 'Користувачі')
@section('h1_title', 'Користувачі')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                <div class="box-body">
                    <table id="users_table" class="table table-bordered table-hover dataTable" role="grid"
                           aria-describedby="tracks_table_info">
                        <thead>
                        <tr role="row">
                            <th class="sorting_desc" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1" >Id Користувача</th>

                            <th class="sorting" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">Ім'я</th>
                            <th class="sorting" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">email</th>
                            <th class="sorting" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">Телефон</th>
                            <th class="sorting" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">Придбано слідів</th>
                            <th tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">
                                
                            </th>
                        </tr>
                        </thead>

                        <tbody>
                        @forelse($users as $user)
                            <tr role="row" class="odd">
                                <td class="sorting_1">{{ $user->id }}</td>

                                <td><a href="{{ route('user.profile', ['id' => $user->id] ) }}">{{ $user->name }}</a></td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->phone }}</td>
                                <td>{{ $user->tracks_count }}</td>
                                <td>
                                    @php
                                    $banRoute = $user->is_ban ? 'user.un-ban' : 'user.ban';
                                    @endphp
                                    {!! Form::model($user, ['route' => [$banRoute, $user->id], 'method' => 'patch']) !!}
                                    <button type="submit" {{ $user->id != Auth::id() ? '' : 'disabled' }}
                                            class="btn {{$user->is_ban ? 'btn-danger btn-ban' : 'btn-success btn-un_ban'}}">
                                        {{!$user->is_ban ? 'Заблокувати' : 'Розблокувати'}}
                                    </button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @empty

                        @endforelse
                        </tbody>

                        <tfoot>
                        <tr role="row">
                            <th class="sorting_desc" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1" >Id Користувача</th>

                            <th class="sorting" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">Ім'я</th>
                            <th class="sorting" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">email</th>
                            <th class="sorting" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">Телефон</th>
                            <th class="sorting" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">Придбано слідів</th>
                            <th tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">

                            </th>
                        </tr>
                        </tfoot>
                    </table>

                </div>

            </div>
        </div>
    </div>

@endsection

@section('inner_js')
    <script>
        $(function () {
            $('#users_table').DataTable({
                'paging'      : true,
                // 'lengthChange': false,
                'searching'   : true,
                // 'ordering'    : true,
                'info'        : false,
                // 'autoWidth'   : false,
                // 'order': [[ 9, 'desc' ]]
            });
        });
    </script>
@stop