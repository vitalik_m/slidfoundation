@extends('layouts.admin')

@section('title', 'Текст')
@section('h1_title', 'Текст')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary container">
                <div class="row">
                    {!! Form::model($text, ['route' => ['text.update', $text->id], 'method' => 'patch', 'enctype' => 'multipart/form-data']) !!}

                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            {!! Form::label('slug', 'Slug:') !!}
                            {!! Form::text('slug', null, ['class' => 'form-control', 'readonly']) !!}
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            {!! Form::label('desc', 'Опис:') !!}
                            {!! Form::text('desc', null, ['class' => 'form-control']) !!}
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="form-group note-editor-page">
                            {!! Form::label('text', 'Зміст:') !!}
                            <textarea class="form-control text-editor" name="content" cols="50" rows="20" id="page-content">
                                {!! $text->content !!}
                            </textarea>
                        </div>
                    </div>

                    <div class="form-group col-xs-12">
                        <input type="submit" value="Оновити тект" class="btn btn-primary">
                    </div>
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </div>

@endsection

@section('inner_js')
    <script>
        $('.text-editor').summernote({
            height: 150,
            width: 300
            }
        );
    </script>
@stop