@extends('layouts.admin')

@section('title', 'Редагування Повідомлення від '.$message->user->name)
@section('h1_title', 'Редагування Повідомлення від '.$message->user->name)

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary container">
                <div class="row">
                    {!! Form::model($message, ['route' => ['message.update', $message->id], 'method' => 'patch', 'enctype' => 'multipart/form-data']) !!}

                    <div class="col-xs-6 col-sm-2">
                        <div class="form-group">
                            {!! Form::label('user.name', 'Користувач:') !!}
                            {{ $message->user->name }}
                        </div>
                    </div>

                    <div class="col-xs-12">
                        <div class="form-group note-editor-page">
                            {!! Form::label('message', 'Повідомлення:') !!}
                            <textarea class="form-control text-editor" name="content" cols="20" rows="10" id="page-content">
                                {!! $message->message !!}
                            </textarea>
                        </div>
                    </div>

                    <div class="form-group col-xs-12">
                        <input type="submit" value="Оновити Повідомлення" class="btn btn-primary">
                    </div>
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </div>

@endsection

@section('inner_js')
    <script>
        $('.text-editor').summernote({
            height: 500
            }
        );
    </script>
@stop