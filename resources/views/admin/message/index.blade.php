@extends('layouts.admin')

@section('title', 'Повідомлення')
@section('h1_title', 'Повідомлення')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                <div class="box-body">
                    <table id="tracks_table" class="table table-bordered table-hover dataTable" role="grid" 
                           aria-describedby="tracks_table_info">
                        <thead>
                        <tr role="row">
                            <th class="sorting_desc" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1" >
                                Id Повідомлення
                            </th>

                            <th class="sorting" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">Користувач</th>
                            <th class="sorting" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">Повідомлення</th>
                            <th class="sorting" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">Створено</th>

                            <th tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">
                                
                            </th>
                        </tr>
                        </thead>

                        <tbody>
                        @forelse($messages as $message)
                            <tr role="row" class="odd">
                                <td class="sorting_1"><a href="{{ route('message.edit', $message->id) }}">{{ $message->id }}</a></td>

                                <td><a href="{{ route('user.profile', $message->user->id) }}">{{ $message->user->name }}</a></td>
                                <td><a href="{{ route('message.edit', $message->id) }}">{{ Str::limit($message->message, 150) }}</a></td>
                                <td>{{ $message->created_at }}</td>
                                <td>
                                    <a href="{{ route('message.edit', $message->id) }}" class="btn btn-default btn-block  btn-xs">
                                        <i class="fa fa-fw fa-edit"></i>
                                    </a>

                                    {!! Form::model($message, ['route' => ['message.destroy', $message->id], 'method' => 'delete', 'enctype' => 'multipart/form-data']) !!}
                                        <button type="submit" class="btn btn-default btn-block  btn-xs">
                                            <i class="fa fa-fw fa-trash"></i>
                                        </button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @empty

                        @endforelse
                        </tbody>

                        <tfoot>
                        <tr role="row">
                            <th class="sorting_desc" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1" >Id Повідомлення</th>

                            <th class="sorting" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">Користувач</th>
                            <th class="sorting" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">Повідомлення</th>
                            <th class="sorting" tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">Створено</th>

                            <th tabindex="0" aria-controls="tracks_table" rowspan="1" colspan="1">

                            </th>
                        </tr>
                        </tfoot>
                    </table>

                </div>

            </div>
        </div>
    </div>

@endsection

@section('inner_js')
    <script>
        $(function () {
            $('#tracks_table').DataTable({
                // 'paging'      : true,
                // 'lengthChange': false,
                'searching'   : false,
                paging: false,
                // 'ordering'    : true,
                'info'        : false,
                // 'autoWidth'   : false,
                // 'order': [[ 9, 'desc' ]]
            });
        });
    </script>
@stop