@inject('markdown', 'Parsedown')
@php($markdown->setSafeMode(true))

@if(isset($reply) && $reply === true)
  <div id="comment-{{ $comment->id }}" class="media">
@else
  <li id="comment-{{ $comment->id }}" class="media">
@endif
    <img src="{{ asset($comment->commenter->photo ? $comment->commenter->photo : $comment->commenter->defaultPhoto) }}" alt="{{ $comment->commenter->name ?? $comment->guest_name }} Avatar">
    <div class="media-body">
        <h5 class="mt-0 mb-1">
            <a href="{{ route('user.profile', $comment->commenter->id) }}">
            {{ $comment->commenter->name ?? $comment->guest_name }}
            </a>
            <small class="text-muted">- {{ $comment->created_at->diffForHumans()}}</small>
        </h5>
        <div style="white-space: pre-wrap;">{!! $markdown->line($comment->comment) !!}</div>

        <div>
            @can('reply-to-comment', $comment)
                <button data-toggle="modal" data-remodal-target="reply-modal-{{ $comment->id }}" class="btn btn-sm btn-link text-uppercase">Відповісти</button>
            @endcan
            @can('edit-comment', $comment)
                <button data-toggle="modal" data-remodal-target="comment-modal-{{ $comment->id }}" class="btn btn-sm btn-link text-uppercase">Редагувати</button>
            @endcan
            @can('delete-comment', $comment)
                <a href="{{ url('comments/' . $comment->id) }}" onclick="event.preventDefault();document.getElementById('comment-delete-form-{{ $comment->id }}').submit();" class="btn btn-sm btn-link text-danger text-uppercase">Видалити</a>
                <form id="comment-delete-form-{{ $comment->id }}" action="{{ url('comments/' . $comment->id) }}" method="POST" style="display: none;">
                    @method('DELETE')
                    @csrf
                </form>
            @endcan
        </div>

        @can('edit-comment', $comment)
            <div class="remodal sl-form-reply-comment " id="comment-modal-{{ $comment->id }}" tabindex="-1" role="dialog"
                 data-remodal-id="comment-modal-{{ $comment->id }}">
                <button data-remodal-action="close" class="sl-form-login__close"></button>

                <div class="sl-form-login__wrap">
                    <div class="f-size-lg-24 f-size-sm-20 f-size-16 f-weight-300 l-height-100">Редагувати коментар</div>

                    <div class="d-flex flex-wrap flex-sm-row flex-column pt-30">
                        <form method="POST" action="{{ url('comments/' . $comment->id) }}">
                            @method('PUT')
                            @csrf
                            <div class="form-group">
                                <label for="message">Оновіть своє повідомлення тут:</label>
                                <textarea required class="form-control" name="message" rows="3"></textarea>
                                {{--<small class="form-text text-muted"><a target="_blank" href="https://help.github.com/articles/basic-writing-and-formatting-syntax">Markdown</a> cheatsheet.</small>--}}
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-sm btn-outline-secondary text-uppercase" data-dismiss="modal">Відмінити</button>
                                <button type="submit" class="btn btn-sm btn-outline-success text-uppercase">Оновити</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endcan

        @can('reply-to-comment', $comment)
            <div class="remodal sl-form-reply-comment " id="reply-modal-{{ $comment->id }}" tabindex="-1" role="dialog"
                  data-remodal-id="reply-modal-{{ $comment->id }}">
                <button data-remodal-action="close" class="sl-form-login__close"></button>

                <div class="sl-form-login__wrap">
                    <div class="f-size-lg-24 f-size-sm-20 f-size-16 f-weight-300 l-height-100">Відповісти на коментар</div>

                    <div class="d-flex flex-wrap flex-sm-row flex-column pt-30">
                        <form method="POST" action="{{ url('comments/' . $comment->id) }}">
                            @csrf
                            <div class="form-group">
                                <label for="message">Введіть свій коментар тут:</label>
                                <textarea required class="form-control" name="message" rows="3"></textarea>
                                {{--<small class="form-text text-muted"><a target="_blank" href="https://help.github.com/articles/basic-writing-and-formatting-syntax">Markdown</a> cheatsheet.</small>--}}
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-sm btn-outline-secondary text-uppercase" data-dismiss="modal">Відмінити</button>
                                <button type="submit" class="btn btn-sm btn-outline-success text-uppercase">Відповісти</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endcan

        <br />{{-- Margin bottom --}}

        {{-- Recursion for children --}}
        @if(array_key_exists($comment->id, $grouped_comments->toArray()))
            @foreach($grouped_comments[$comment->id] as $child)
                @include('comments::_comment', [
                    'comment' => $child,
                    'reply' => true,
                    'grouped_comments' => $grouped_comments
                ])
            @endforeach
        @endif

    </div>
@if(isset($reply) && $reply === true)
  </div>
@else
  </li>
@endif