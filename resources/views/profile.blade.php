@extends('layouts.app')

@section('content')
    <div class="sl-container sl-office-page">
        <div class="container-fluid mb-35">
            <div class="sl-office-page-col">
                <div class="d-flex flex-wrap align-items-start">
                    <div class="d-flex align-items-center sl-office-page__col-page px-0 js_form">
                        <div class="sl-office-page__img">
                            <img id="preview-img" src="{{ asset($user->photo ? $user->photo : $user->defaultPhoto) }}" alt="">
                            @if($user->isOwner())
                            <div id="js__edit-button__container-photo" class="edit-button__container sl-office-page__container sl-office-page__icon">
                                <img class="edit-button__icon" src="{{ asset('images/photo_edit.png') }}" alt="pencil">
                            </div>
                            @endif
                        </div>
                        @if($user->isOwner())
                            {!! Form::model($user, ['route' => ['user.update', $user->id],
                                'method' => 'patch', 'enctype' => 'multipart/form-data',
                                'class' => 'upload-photo js__update-photo', 'style' => 'display: none;']) !!}

                                {!! Form::file('photo', ['class' => 'form-control image-upload']) !!}
                                <div class="form-group col-xs-12">
                                    <input type="submit" value="Змінити фото" class="btn btn-save">
                                </div>

                            {!! Form::close() !!}
                        @endif

                        <div class="sl-office-page__info">
                            <div class="d-flex align-items-center mb-sm-10 mb-5 pos-relative js_form">
                                @if($user->countryCod && file_exists(public_path('/images/24/'.$user->countryCod.'.png')))
                                <img class="sl-office-page__flag" src="{{ asset('/images/24/'. $user->countryCod .'.png') }}" alt="">
                                @endif

                                <div class="f-size-lg-36 f-size-md-26 f-size-20 l-height-100 f-weight-300 ml-10 js_user-name">{{ $user->name }}</div>
                                @if($user->isOwner())
                                    <div id="js__edit-button__container" class="edit-button__container sl-office-page__container"><img class="edit-button__icon" src="../../images/profile_edit.png" alt="pencil"></div>
                                    {!! Form::model($user, ['route' => ['user.update', $user->id], 'method' => 'patch',
                                        'class' => 'js__update-name update-name align-items-center mb-sm-10 mb-5', 'style' => 'display: none;']) !!}

                                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                                    <div class="form-group col-xs-12 name-change">
                                        <input type="submit" value="Змінити Им'я" class="btn btn-save">
                                    </div>
                                    {!! Form::close() !!}
                                @endif
                            </div>

                            <div class="sl-office-page__statistic">
                                <div class="sl-office-page__statistic-tracks">{{ $user->tracks_count }}</div>
                                <div class="sl-office-page__statistic-rating">{{ $user->rating() }}</div>
                            </div>

                            
                        </div>
                    </div>
                </div>
                <div class="d-flex align-items-center mb-sm-10 mb-5 phone-email-container">
                    <div class="py-5 py-5__flex">
                        @if($user->isOwner() || $user->phoneIsPublic)
                        <a href="tel:{{ $user->phone }}" class="sl-office-page__contact sl-office-page__contact_tel js__cellphone">
                            {{ $user->phone}}</a>
                        @endif

                        @if($user->isOwner())
                            {!! Form::model($user, ['route' => ['user.update', $user->id], 'method' => 'patch',
                                'class' => 'update-phone', 'style' => 'display: none;']) !!}

                            {!! Form::text('phone', null, ['class' => 'form-control']) !!}
                            <div class="form-group col-xs-12">
                                <input type="submit" value="Змінити телефон" class="btn btn-save">
                            </div>
                            {!! Form::close() !!}
                        @endif
                    </div>

                    @if($user->isOwner())
                    <div class="py-5 py-5__flex user-colors-block">
                        <div class="sl-office-page__contact_map_mod">
                            {!! Form::model($user, ['route' => ['user.update', $user->id], 'method' => 'patch',
                                'class' => 'update-map_mod']) !!}

                                <select name='map_mod' class="form-control input-map_mod">
                                    @foreach($user::MAP_MODS as $mod)
                                    <option value="{{$mod}}" {{ Auth::user()->map_mod == $mod ? 'selected' : '' }}>
                                        {{$mod}}
                                    </option>
                                    @endforeach
                                </select>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    @endif

                    <div class="py-5">
                        @if($user->isOwner() || $user->mailIsPublic)
                        <a href="mail:{{ $user->email }}" class="sl-office-page__contact sl-office-page__contact_mail">{{ $user->email }}</a>
                        @endif
                    </div>

                    @if($user->isOwner())
                        <div id="js__edit-button__container-cellphone" class="py-5__container"><img class="edit-button__icon" src="../../images/profile_edit.png" alt="pencil"></div>
                    @endif
                </div>
                            
                <hr class="hr-style-3 my-lg-30 my-20">

                <div class="sl-title-line-dark f-size-lg-24 f-size-md-22 f-size-18 l-height-120 f-weight-300 mb-20">Про себе
                    @if($user->isOwner())
                    <div id="js__edit-button__container-about" class="edit-button__container sl-office-page__container">
                        <img class="edit-button__icon" src="../../images/profile_edit.png" alt="pencil">
                    </div>
                    @endif
                </div>
                <div class="f-size-md-14 f-size-13 l-height-140 f-weight-300 js__about">
                    {!! $user->about !!}
                </div>
                @if($user->isOwner())
                    {!! Form::model($user, ['route' => ['user.update', $user->id], 'method' => 'patch',
                        'class' => 'update-about js__about-form', 'style' => 'display: none;']) !!}
                    <textarea class="form-control text-editor" name="about" cols="50" rows="10" id="value-text">
                        {!!  $user->about !!}
                    </textarea>
                    <div class="form-group col-xs-12">
                        <input type="submit" value="Змінити Про себе" class="btn btn-save">
                    </div>
                    {!! Form::close() !!}
                @endif


                <hr class="hr-style-3 mt-lg-30 mb-lg-20 my-20">


                <div class="sl-title-line-dark f-size-lg-24 f-size-md-22 f-size-18 l-height-120 f-weight-300 mb-20">Iсторiя замовлень
                    @if($user->isOwner())
                    {!! Form::model($user, ['route' => ['user.ajax-update', $user->id], 'method' => 'patch',
                        'class' => 'update-trackIsPublic js__about-form']) !!}
                        {{ Form::checkbox('trackIsPublic', null , null, ['class' => 'checkbox input-trackIsPublic', 'id' => 'checkbox']) }}
                        <label for="checkbox" class="f-size-lg-24 f-size-md-22 f-size-18">Відображати усім</label>
                    {!! Form::close() !!}
                    @endif
                </div>

                @if($tracks)
                    @foreach($tracks as $track)
                <div class="sl-office-page__purchase-history js__card-history">
                    <div class="col-sm col-12 px-0 d-flex flex-column">
                        <div class="f-size-lg-24 f-size-md-22 f-size-18 l-height-120 f-weight-300">
                            {{ $track->title ? $track->title : '#'.$track->id }}
                        </div>
                        <div class="f-size-md-14 f-size-13 l-height-120 f-weight-300">{{ $track->created_at }}</div>
                        <div class="f-size-md-16 f-size-15 l-height-120 f-weight-700 brand-primary-color mt-sm-35 mt-10">
                            {{ !empty($track->amount) ? $track->amount : '0'}} грн
                        </div>

                        @if($user->isOwner())
                        {!! Form::model($track, ['route' => ['user.track-update', 'user' => $user->id, 'track' => $track->id],
                                        'method' => 'patch', 'class' => 'track-update card-form', 'style' => 'display: none;', 'enctype' => 'multipart/form-data']) !!}
                            <p class="card-title f-size-md-14 f-size-13 l-height-120 f-weight-300">Заголовок сліду</p>
                            {!! Form::text('title', null, ['class' => 'form-control']) !!}
                            <p class="card-title f-size-md-14 f-size-13 l-height-120 f-weight-300">Опис сліду</p>
                            <textarea class="form-control text-editor" name="content" cols="50" rows="10" id="value-text">
                                {!!  $track->content !!}
                            </textarea>
                            <p class="card-title f-size-md-14 f-size-13 l-height-120 f-weight-300">Відео сліду</p>
                            @if($track->video)
                                <div class="">
                                    <iframe width="100%" height="auto" src="{{ $track->video }}"
                                            frameborder="0"
                                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                            allowfullscreen></iframe>
                                </div>
                            @endif
                            {!! Form::text('video', null, ['class' => 'form-control']) !!}
                            <p class="card-title f-size-md-14 f-size-13 l-height-120 f-weight-300">Зображення сліду</p>
                            <div class="sl-office-page__card-img">
                                <img id="preview-img" src="{{ asset($track->img ? $track->img : $track::DEFAULT_IMG) }}" alt="">
                            </div>
                            {!! Form::file('img', ['class' => 'form-control image-upload']) !!}

                            <div class="form-group col-xs-12">
                                <input type="submit" value="Оновити слід" class="btn btn-save">
                            </div>
                        {!! Form::close() !!}
                        @endif
                    </div>
                    <div class="col-sm-auto col-12 px-0 text-sm-right mt-sm-0 mt-10 flex-col-end">
                        <a class="sl-office-page__purchase-history-link"
                           href="{{route('home', ['lon' => $track->lon, 'lat' => $track->lat] )}}">
                            Знайти на мапі
                        </a>

                        @if($user->isOwner())
                        <a class="sl-office-page__purchase-history-link js__edit-card"
                           href="#">
                            Редагувати опис
                        </a>
                        @endif()
                    </div>
                </div>
                    @endforeach

                    {{ $tracks->render() }}
                @endif

            </div>
        </div>
    </div>
@endsection

@section('inner_js')
    <script>
        $('.text-editor').summernote({
                height: 250
            }
        );

        $('.input-trackIsPublic').first().change((e) => {
            let form = e.target.parentElement;

            var url = $(form).attr('action');
            var data = $(form).serialize();

            $.post(url, data, function(result) {
                a =result;
            }).fail(function (result){
                a =result;
            });
        });

        $('.input-map_mod').first().change((e) => {
            let form = e.target.parentElement;

            var url = $(form).attr('action');
            var data = $(form).serialize();

            $.post(url, data, function(result) {
                a =result;
            }).fail(function (result){
                a =result;
            });
        });
    </script>
@stop