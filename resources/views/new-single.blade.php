@extends('layouts.app')

@section('content')
    <div class="sl-container sl-page">
        <div class="container-fluid mb-35">
            <div class="sl-page-col">
                <h1 class="sl-title-line-red f-size-lg-36 f-size-md-32 f-size-28 l-height-100 f-weight-300 mb-30">
                    {{ $newArticle->title }}
                </h1>
                <div class="f-size-md-14 f-size-13 f-weight-300 l-height-140 clearfix">
                    {!!  $newArticle->content  !!}
                </div>

                <div class="sl-sharer">
                    @if(env('FACEBOOK_LINK'))
                    <button class="sl-sharer__facebook" data-sharer="facebook" data-hashtag="hashtag"
                            data-url="{!! env('FACEBOOK_LINK') !!}"></button>
                    @endif

                    @if(env('TWITTER_LINK'))
                    <button class="sl-sharer__twitter" data-sharer="twitter" data-title="Checkout Sharer.js!"
                            data-hashtags="awesome, sharer.js" data-url="{!! env('TWITTER_LINK') !!}"></button>
                    @endif

                    @if(env('INSTAPAPER_LINK'))
                    <button class="sl-sharer__instapaper" data-sharer="instapaper" data-title="Checkout Sharer.js!"
                            data-description="Awesome lib!" data-url="{!! env('INSTAPAPER_LINK') !!}"></button>
                    @endif
                </div>

            </div>

        </div>
        <div class="container-fluid mb-35 comments-block">
            @comments(['model' => $newArticle])
        </div>
    </div>
@endsection

@section('inner_js')
@stop