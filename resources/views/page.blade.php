@extends('layouts.app')

@section('content')
    <div class="sl-container sl-office-page">
        <div class="container-fluid mb-35">
            <div class="sl-office-page-col">

                <h1 class="sl-title-line-red f-size-lg-36 f-size-md-32 f-size-28 l-height-100 f-weight-300 mb-30">{{$page->title}}</h1>

                <div class="f-size-md-14 f-size-13 f-weight-300 l-height-140 clearfix page-content page-about">
                {!! $page->content !!}
                </div>

            </div>
        </div>
    </div>
@endsection

@section('inner_js')
@stop