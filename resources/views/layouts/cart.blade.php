<div class="sl-form-login__wrap" id="cart">
    <div class="f-size-lg-24 f-size-sm-20 f-size-16 f-weight-300 l-height-100">Ваша корзина</div>
    {!! Form::model(null, ['route' => ['order.ajax-create'], 'method' => 'post', 'class' => 'items-cart-form']) !!}
        <div class="d-flex flex-column pt-30">

            @if(Auth::user() && Auth::user()->activeReserve)
                @foreach(Auth::user()->reservedTracks() as $track)
            <div class="d-flex flex-wrap cart-pos items-cart-form">
                <input type="hidden" name="track_id" value="{{$track->id}}">
                <p class="cart-pos__title">{{ $track->title .' #'. $track->id }}</p>
                <p class="cart-pos__price">
                    <span>{{$track->amount}}</span> грн
                </p>
                <span class="chatbox-container__close cart-pos__delete js__cart-pos__delete"></span>
            </div>
                @endforeach
            @endif

        </div>

        <div class="d-flex flex-wrap cart-total">
            <p class="cart-total__title">Загальна вартість</p>
            <p class="cart-total__price"><span>0</span> грн</p>
        </div>
        {!! Form::hidden('reserve_id', !empty($track) ? $track->reserve_id : null) !!}
        <div class="d-flex mt-30 align-items-center">
            <button class="button-buy">Придбати</button>
        </div>
    {!! Form::close() !!}

    {!! Form::model(null, ['route' => ['track.ajax-unreserve'], 'method' => 'patch', 'id' => 'del-cart-item']) !!}
        {!! Form::hidden('reserve_id', !empty($track) ? $track->reserve_id : null) !!}
        {!! Form::hidden('track', null) !!}
    {!! Form::close() !!}
</div>