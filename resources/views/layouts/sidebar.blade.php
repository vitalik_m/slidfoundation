<div class="sl-sidebar">
    <div class="sl-sidebar-open"></div>
    <div class="sl-sidebar__scrollbar">
        <a href="{{ route("home") }}" class="sl-sidebar__logo">
            <img src="{{ asset('/images/logo.png')}}" alt="">
        </a>
        <hr class="hr-style-1">
        <div class="d-flex justify-content-between f-size-16">
            <div class="f-weight-300">Кількість слідів</div>
            <div>
                <span class="brand-primary-color">{{ intval($sideMenuData['tracks']['bought']) }}</span> / {{ $sideMenuData['tracks']['all'] }}
            </div>
        </div>
        <hr class="hr-style-1">

        @if($sideMenuData['users']->count() > 0)
        <div class="d-flex justify-content-between mb-20">
            <div class="f-weight-300 f-size-16">Мiсце у рейтингу</div>
            <div class="f-size-14">
                <a href="{{ route('rating') }}">всі користувачі</a>
            </div>
        </div>
        <hr class="hr-style-2">
        <div class="sl-rating">
            <div class="scrollbar-inner">

                @foreach($sideMenuData['users'] as $key => $user)
                <div class="sl-rating__item {{ $key%2 ? 'sl-rating__item_odd' :'' }}">
                    <div class="sl-rating__item-img">
                        <div class="sl-img">
                            <img src="{{ asset($user->photo ? $user->photo : $user->defaultPhoto) }}" alt="">
                        </div>
                    </div>
                    <div class="sl-rating__item-content">
                        <div class="d-flex flex-wrap align-items-center w-100">
                            @if($user->countryCod && file_exists(public_path('/images/24/'.$user->countryCod.'.png')))
                                <img class="sl-flag" src="{{ asset('/images/24/'. $user->countryCod .'.png') }}" alt="">
                            @endif
                            <span class="ml-10 f-size-14 l-height-100"><a href="{{ route('user.profile', $user->id) }}">{{$user->name }}</a></span>
                        </div>
                        <div class="d-flex align-items-center w-100">
                            <div class="sl-content-tracks">{{ $user->tracks_count ? $user->tracks_count : '0'}}</div>
                            <div class="sl-content-rating">{{$key+1 }}</div>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
            <div class="external-scroll_y">
                <div class="scroll-element_outer">
                    <div class="scroll-element_size"></div>
                    <div class="scroll-element_track"></div>
                    <div class="scroll-bar"></div>
                </div>
            </div>
        </div>
        @endif

        @if(!$lastShortNews->isEmpty())
        <hr class="hr-style-2">

        <div class="f-size-16 f-weight-300 mt-20">Новини</div>
        <div class="sl-news">
            @foreach($lastShortNews as $lastShortNew)
            <div class="sl-news__item">
                <a href="{{route('news')}}">{{$lastShortNew->content}}</a>
                <div class="d-flex justify-content-between align-items-center mt-10">
                    <div class="sl-news__data f-size-10 l-height-100">
                        {{ \Carbon\Carbon::instance($lastShortNew->created_at)->format('d.m.Y') }}</div>
                    {{--<div class="d-flex align-items-center">--}}
                        {{--<div class="sl-views">245</div>--}}
                        {{--<div class="sl-comments">362</div>--}}
                    {{--</div>--}}
                </div>
            </div>
            @endforeach
        </div>
        @endif

        <div class="sl-social">
            @if(env('YOUTUBE_LINK'))
                <a href="{!! env('YOUTUBE_LINK') !!}" class="sl-social__link sl-social__link_youtube"></a>
            @endif
            @if(env('INSTAGRAM_LINK'))
                <a href="{!! env('INSTAGRAM_LINK') !!}" class="sl-social__link sl-social__link_inst"></a>
            @endif
        </div>

        <div class="sl-copyright f-size-10 f-weight-300 l-height-120">
            &copy; «Слiди на дорозi» {{ \Carbon\Carbon::now()->year }}
        </div>

    </div>
    <div class="sl-sidebar_external-scroll_y">
        <div class="scroll-element_outer">
            <div class="scroll-element_size"></div>
            <div class="scroll-element_track"></div>
            <div class="scroll-bar"></div>
        </div>
    </div>
</div>