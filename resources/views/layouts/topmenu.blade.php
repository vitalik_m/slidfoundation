<div class="sl-adminbar">
    <div class="sl-adminbar__admin order-md-1 d-flex">
        @if(!Auth::user())
        <div class="sl-adminbar__login">
            <a data-remodal-target="login" href="{{ route('login') }}">Увійдіть в особистий кабінет</a>
        </div>
        @else
        <div class="sl-adminbar__personal-area cart-block">
            <a data-remodal-target="cart" id='open_cart_btn' href=""
               style="{{ Auth::user()->activeReserve ? '': 'display: none;' }}"></a>
        </div>
        <div class="sl-adminbar__personal-area login-block">
            Вітаємо,
            <div class="sl-adminbar__personal-area-drop">
                <a href="#">{{ Auth::user()->name ? Auth::user()->name : 'User #'.Auth::user()->id }}</a>
                <ul>
                    @if(Auth::user()->canAdmin())
                    <li><a href="{{ route('admin') }}">Admin</a></li>
                    @endif
                    @if(Route::currentRouteName() != 'profile')
                    <li><a href="{{ route('user.profile', Auth::user()->id) }}">Особистий кабiнет</a></li>
                    @endif

                    <li>
                        <a class="" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            Вийти
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                              style="display: none;">
                            @csrf
                        </form>

                    </li>
                </ul>
            </div>
        </div>
        @endif
    </div>

    <div class="sl-adminbar__nav order-md-0">
        <a href="{{ route('page', ['slug' => 'about']) }}" class="{{ Request::url() == route('page', ['slug' => 'about']) ? 'active' : '' }}">
            Про проект</a>
        <a href="{{ route('news') }}" class="{{ Request::url() == route('page', ['slug' => 'news']) ? 'active' : '' }}">
            Новини</a>
        <a href="{{ route('page', ['slug' => 'contact']) }}" class="{{ Request::url() == route('page', ['slug' => 'contact']) ? 'active' : '' }}">
            Контакти</a>
    </div>
</div>