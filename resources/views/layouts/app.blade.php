<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'SlidFoundation') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap&subset=cyrillic" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/remodal/css/remodal.css') }}">
    <link rel="stylesheet" href="{{ asset('css/spacing.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/styles-new.css') }}">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-143152210-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-143152210-1');
    </script>
</head>
<body>
<div id="fb-root"></div>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous"
        src="https://connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v3.3&appId=638398169919497&autoLogAppEvents=1"></script>
<div id="app">
    @include('layouts.sidebar')

        <main class="main-wrapper">
        @include('layouts.topmenu')

        @foreach ($errors->all(':message') as $input_error)
            @include('popups.info', ['text' => $input_error, 'show' => true])
        @endforeach

        @yield('content')
        </main>

        <chat :user="{{ auth()->id() ? auth()->user() : 'null' }}"></chat>

    <div class="remodal sl-form-login" data-remodal-id="login" data-remodal-options="hashTracking: false, closeOnOutsideClick: false">
        <button data-remodal-action="close" class="sl-form-login__close"></button>
        @include('auth.login_form')
    </div>

    <div class="remodal sl-form-register" data-remodal-id="register" data-remodal-options="hashTracking: false, closeOnOutsideClick: false">
        <button data-remodal-action="close" class="sl-form-register__close"></button>
        @include('auth.register_form')
    </div>

        <div id="cart" class="remodal sl-form-cart" data-remodal-id="cart" data-remodal-options="hashTracking: false">
            <button data-remodal-action="close" class="sl-form-login__close"></button>
            @include('layouts.cart')
        </div>

        @include('popups.info')
    </div>

<script src="{{ asset('/js/app.js')}}"></script>
{{--    <script src="{{ asset('vendor/jquery/js/jquery.min.js') }}"></script>--}}
<script id="widget-wfp-script" language="javascript" type="text/javascript"
        src="https://secure.wayforpay.com/server/pay-widget.js"></script>
    <script src="{{ asset('vendor/sharer/js/sharer.min.js') }}"></script>
    <script src="{{ asset('vendor/remodal/js/remodal.min.js') }}"></script>
    <script src="{{ asset('js/jquery.scrollbar.js') }}"></script>
    <script src="{{ asset('js/script.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.js"></script>
    <script src="{{ asset('/js/common.js')}}"></script>
    <script src="{{ asset('/js/chat.js')}}"></script>
    <script src="{{ asset('/js/profile_page.js')}}"></script>
    @yield('inner_js')

    <script type="application/javascript">
        var routes = {
            unReserve : '{{route('track.ajax-unreserve')}}',
            orderCreate : '{{route('order.ajax-create')}}',
            orderApprove : '{{route('order.ajax-approve')}}',
            orderDecline : '{{route('order.ajax-decline')}}',
        }
    </script>

    @if(!$errors->isEmpty())
    <script>
        $(document).ready(()=>{
            var inst = $('[data-remodal-id=register]').remodal({'closeOnOutsideClick' : false});

            inst.open();
        })
    </script>
    @endif
</body>
</html>
