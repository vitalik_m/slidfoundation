
    <p class="popup-buy__title popup-text">{{ $track->title .' #'. $track->id }}</p>
    <p class="popup-buy__price popup-text">Вартість: {{ $track->amount ? $track->amount : '0'}}грн</p>
    <div class="popup-buy__img-container">
        @if($track->video)
            <iframe width="100%" height="auto" src="{{ $track->video }}"
                    frameborder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>
        @endif
            <img class="popup-buy__img" src="{{ asset($track->getImg()) }}" alt="">
    </div>
    <p class="popup-buy__description popup-text">
        @if($track->content)
            {{ $track->content }}
        @else
            <span class="text-reserved" style="{{!$track->isReserved()?'display:none':''}}">{{ $texts['popup_track_reserved'] }}</span>
            <span class="text-free" style="{{$track->isReserved()?'display:none':''}}">{{ $texts['popup_track_free'] }}</span>
        @endif
    </p>
    <div class="popup-buy__btns-container" id="trackPopupBtns-{{$track->id}}">
        @php $styles = $track->isReserved() ? 'display: none;' : '';@endphp
        {!! Form::model(null, ['route' => ['track.ajax-reserve', $track->id], 'method' => 'patch',
            'class' => 'buy-form ', 'style' => $styles]) !!}

            {!! Form::hidden('user', \Auth::id()) !!}
            {!! Form::hidden('reserve', $reserve_id ) !!}
            {!! Form::hidden('track', $track->id) !!}

            <button class="button-buy__book">Забронювати</button>
            <p class="popup-buy__choise popup-text">або</p>
            <button class="button-buy__later">купити зараз</button>
        {!! Form::close() !!}
        <div class="reserved-block" style="{{ !$track->isReserved() ? 'display:none;' : '' }}">
            @if($track->isReserved() && Auth::id() && Auth::id() == Auth::user()::ADMIN_ID)
                @if($track->reserve->isActual() == 'time')
                <p><em>{{$track->reserve->user->name}}</em> забронював цей слід</p>
                @else
                <p>Обробка платежу від <em>{{$track->reserve->user->name}}</em></p>
                @endif
            @endif
            <button class="button-buy__book reserved" disabled>
                    Заброньовано
            </button>
        </div>
    </div>