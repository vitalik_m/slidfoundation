
    <div class="popup-owner__info-container">
        <div class="popup-owner__img-container">
            <img class="popup-owner__img" src="{{ asset($track->owner->getPhoto()) }}" alt="avatar">
        </div>
        <div class="popup-owner__info">
            <div class="name-img-container">
                @if($track->owner->countryCod && file_exists(public_path('/images/24/'.$track->owner->countryCod.'.png')))
                    <img class="sl-office-page__flag" src="{{ asset('/images/24/'. $track->owner->countryCod .'.png') }}" alt="country">
                @endif
                <a href="{{ route('user.profile', $track->owner->id) }}" class="popup-owner__info-name">{{ $track->owner->name }}</a>
            </div>
            <div class="sl-office-page__statistic">
                <div class="sl-office-page__statistic-tracks">{{ $track->owner->tracks_count }}</div>
                <div class="sl-office-page__statistic-rating">{{ $track->owner->rating() }}</div>
            </div>
            <h3>{{$track->title . ' #' . $track->id}}</h3>
        </div>
    </div>
    <div class="popup-owner__content-container">
        @if($track->video)
            <iframe width="100%" height="auto" src="{{ $track->video }}"
                    frameborder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>
        @else
            <img class="popup-buy__img" src="{{ asset($track->getImg()) }}" alt="">
        @endif
    </div>
    <p class="popup-owner__description popup-text">
        @if($track->status == $track->statuses['checked'] && $track->content)
            {!! $track->content !!}
        @elseif($track->content)
        @endif
    </p>
    <div class="sl-sidebar_external-scroll_y">
        <div class="scroll-element_outer">
            <div class="scroll-element_size"></div>
            <div class="scroll-element_track"></div>
            <div class="scroll-bar"></div>
        </div>
    </div>