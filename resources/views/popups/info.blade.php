<div id="js__inform-popup" class="popup-inform popup-basic" {!! !empty($show) ? 'style="display:block"': ''  !!} >
    <span class="chatbox-container__close popup-buy__close js__popup-inform"></span>
    <p class="popup-inform__title popup-text">{{ !empty($title) ? $title: '' }}</p>
    <div class="popup-inform__description popup-text">{{ !empty($text) ? $text: '' }}</div>
</div>