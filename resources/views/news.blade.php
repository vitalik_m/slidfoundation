@extends('layouts.app')

@section('content')
<div class="sl-container sl-news-page">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <div class="sl-title-line-red f-size-lg-36 f-size-md-32 f-size-28 l-height-100 f-weight-300">Новини проекту</div>

                @forelse($news as $new)
                <div class="sl-news-page__item">
                    <div class="sl-news-page__item-data text-center">
                        <strong class="f-size-lg-24 f-size-20">
                            {{ \Carbon\Carbon::instance($new->created_at)->day }}
                        </strong>
                        <div class="f-size-14 f-weight-300">
                            {{ $month[\Carbon\Carbon::instance($new->created_at)->month-1] }}
                        </div>
                    </div>
                    <div class="sl-news-page__item-content">
                        <h4 class="f-size-lg-24 f-size-md-20 f-size-17 l-height-120 f-weight-300 mb-md-20 mb-10">
                            <a href="{{ route('single-new', ['id' =>$new->id ]) }}">{{ $new->title }}</a>
                        </h4>
                        <div class="f-size-sm-14 f-size-12 l-height-140 f-weight-300">
                            {{ Str::limit($new->content, 300) }}
                        </div>
                    </div>
                </div>
                @empty
                    Новини незабаром з'являться
                @endforelse

            </div>

            <div class="col-xl-3 col-lg-5">
                <div class="sl-title-line-dark f-size-lg-24 f-size-md-22 f-size-18 l-height-120 f-weight-300 mb-20">Швидкi новини</div>

                @forelse($shortNews as $shortNew)
                    <div class="sl-news-page__min-item">
                        <div class="sl-news-page__min-item-time">{{ \Carbon\Carbon::instance($shortNew->created_at)->format('h:i') }}</div>
                        <div class="f-size-14 l-height-140 f-weight-300 pt-0 mb-0">
                            {{ $shortNew->content }}
                        </div>
                    </div>
                @empty
                    Швидкі новини незабаром з'являться
                @endforelse

            </div>
        </div>
    </div>
</div>
@endsection

@section('inner_js')
@stop