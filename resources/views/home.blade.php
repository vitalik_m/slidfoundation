@extends('layouts.app')

@section('content')
        <div class="sl-container">
            <div id="ico_tracks_loader" class="loading" style="display: none"></div>
            <div class="sl-map">
                <div class="map" id="makemap_canvas" style="height: 100%;"></div>
            </div>

            <div class="current-info">
                <div class="time">День {{$currentInfo['day']}}, час {{$currentInfo['time']}}</div>
                <div class="place">Місце знаходження: <a href="#">{{$currentInfo['place']}}</a></div>
            </div>
        </div>
@endsection
@section('inner_js')
    <script>
        var map;
        var markers = [];
        var texts = {!! $texts !!};
        var map_data = {
            tracks : {!! $tracks !!},
            centerPosition : {'lat' : '{{ $center['lat'] }}', 'lon' : '{{ $center['lon'] }}'},
            currentPosition : {'id' : {{ $curPosition['id'] }}, 'lat' : {{ $curPosition['lat'] }}, 'lon' : {{ $curPosition['lon'] }}},
            userMapTheme : '{{ Auth::id() ? Auth::user()->map_mod : 'auto' }}',
            nightStyleMap : [
                {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
                {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
                {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
                {
                    featureType: 'administrative.locality',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#d59563'}]
                },
                {
                    featureType: 'poi',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#d59563'}]
                },
                {
                    featureType: 'poi.park',
                    elementType: 'geometry',
                    stylers: [{color: '#263c3f'}]
                },
                {
                    featureType: 'poi.park',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#6b9a76'}]
                },
                {
                    featureType: 'road',
                    elementType: 'geometry',
                    stylers: [{color: '#38414e'}]
                },
                {
                    featureType: 'road',
                    elementType: 'geometry.stroke',
                    stylers: [{color: '#212a37'}]
                },
                {
                    featureType: 'road',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#9ca5b3'}]
                },
                {
                    featureType: 'road.highway',
                    elementType: 'geometry',
                    stylers: [{color: '#746855'}]
                },
                {
                    featureType: 'road.highway',
                    elementType: 'geometry.stroke',
                    stylers: [{color: '#1f2835'}]
                },
                {
                    featureType: 'road.highway',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#f3d19c'}]
                },
                {
                    featureType: 'transit',
                    elementType: 'geometry',
                    stylers: [{color: '#2f3948'}]
                },
                {
                    featureType: 'transit.station',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#d59563'}]
                },
                {
                    featureType: 'water',
                    elementType: 'geometry',
                    stylers: [{color: '#17263c'}]
                },
                {
                    featureType: 'water',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#515c6d'}]
                },
                {
                    featureType: 'water',
                    elementType: 'labels.text.stroke',
                    stylers: [{color: '#17263c'}]
                }
            ],
            trackPopupRoute : '{{route('home.ajax-track-popup2')}}',
            updateCurrentPositionRoute : '{{route('home.ajax-update-position')}}',
            updateTracksByBounds : '{{route('home.ajax-tracks-by-bounds')}}',
            homePageRoute : '{{route('home')}}',
            isAdmin : '{{ Auth::user() ? Auth::user()->isTracking() : '' }}',
        };
    </script>
    <script src="{{ asset('/js/map.js')}}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAP_ID') }}&callback=slid_map.init"
            async defer></script>
@stop