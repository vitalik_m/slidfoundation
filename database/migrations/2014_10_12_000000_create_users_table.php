<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('email')->unique()->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->string('provider')->nullable();
            $table->string('provider_id')->nullable();

            $table->string('phone')->nullable();
            $table->integer('tracks_count')->nullable();

            $table->boolean('phoneIsPublic')->default('0');
            $table->boolean('emailIsPublic')->default('0');
            $table->boolean('trackIsPublic')->default('1');

            $table->string('countryCod')->nullable();
            $table->string('photo')->nullable();
            $table->text('about')->nullable();

            $table->rememberToken()->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
