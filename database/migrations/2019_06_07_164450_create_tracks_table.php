<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTracksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tracks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('lat', 10, 6);
            $table->float('lon', 10, 6);
            $table->string('title')->nullable();
            $table->text('content')->nullable();
            $table->string('img')->nullable();
            $table->string('video')->nullable();
            $table->float('amount')->default('5');
            $table->integer('user_id')->default('1');
            $table->enum('status', ['pending', 'checked', 'deny'])->default('pending');
            $table->boolean('isCurrent')->default(0);
            $table->bigInteger('reserve_id')->nullable();
            $table->bigInteger('order_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tracks');
    }
}
