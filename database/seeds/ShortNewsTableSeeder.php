<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ShortNewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('short_news')->insert([
            'id' => 1,
            'content' => '"Почали співпрацю не з дуже гарного листка": у Зеленського прокоментували політичну кризу у відносинах з Радою',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
