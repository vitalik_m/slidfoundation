<?php

use Illuminate\Database\Seeder;

class ReservesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('reserves')->insert([
            'id' => 1,
            'user_id' => '3',
            'created_at' => '2019-06-10 20:13:00',
            'updated_at' => '2019-06-10 20:13:00',
        ]);
    }
}
