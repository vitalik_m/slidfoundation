<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => 1,
            'name' => 'Валерій Ананьєв',
            'email' => 'admin@gmail.com',
            'phone' => '0667788999',
            'password' => bcrypt('admin'),
            'countryCod' => 'UA',
            'tracks_count' => '0',
        ]);

        if(env('APP_ENV') == 'local') {
            DB::table('users')->insert([
                'id' => 2,
                'name' => 'Test mid user',
                'email' => 'test1@gmail.com',
                'phone' => '0667788999',
                'password' => bcrypt('admin'),
                'countryCod' => 'BG',
                'tracks_count' => '2',
            ]);
            DB::table('users')->insert([
                'id' => 3,
                'name' => 'Test top user',
                'email' => 'test2@gmail.com',
                'phone' => '0667788999',
                'password' => bcrypt('admin'),
                'countryCod' => 'PL',
                'tracks_count' => '3',
            ]);
        }
    }
}
