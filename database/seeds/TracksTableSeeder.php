<?php

use Illuminate\Database\Seeder;

class TracksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tracks')->insert([
            'id' => 2,
            'title' => 'Test bought Track',
            'lat' => '59.907300',
            'lon' => '10.780060',
            'status' => 'pending',
            'amount' => '1200',
            'user_id' => '2'
        ]);
        DB::table('tracks')->insert([
            'id' => 6,
            'title' => 'Test bought Track',
            'lat' => '59.907300',
            'lon' => '10.780100',
            'status' => 'pending',
            'amount' => '3500',
            'user_id' => '2'
        ]);


        DB::table('tracks')->insert([
            'id' => 3,
            'title' => 'Test bought Track',
            'lat' => '59.9072900',
            'lon' => '10.780060',
            'status' => 'pending',
            'amount' => '3200',
            'user_id' => '3'
        ]);
        DB::table('tracks')->insert([
            'id' => 4,
            'title' => 'Test bought Track',
            'lat' => '59.907318',
            'lon' => '10.780034',
            'status' => 'pending',
            'amount' => '200',
            'user_id' => '3'
        ]);
        DB::table('tracks')->insert([
            'id' => 5,
            'title' => 'Test bought Track',
            'lat' => '59.907320',
            'lon' => '10.780060',
            'status' => 'pending',
            'amount' => '100',
            'user_id' => '3'
        ]);
        DB::table('tracks')->insert([
            'id' => 7,
            'title' => 'Test reserve Track',
            'lat' => '59.907420',
            'lon' => '10.780160',
            'status' => 'pending',
            'amount' => '100',
            'reserve_id' => '1'
        ]);
    }
}
