<?php

use Illuminate\Database\Seeder;

class TextsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('texts')->insert([
            'id' => 1,
            'slug' => 'chat-without-tracks',
            'desc' => 'Користувач без придбаних слідів намагаэтся слати повідомлення у чат',
            'text' => 'купи слід',
        ]);
        DB::table('texts')->insert([
            'id' => 2,
            'slug' => 'buy-tracks',
            'desc' => 'Користувач придбав слід(и)',
            'text' => '<p>Тепер Ви маєте змогу редагувати інформацію на 
                            <a href="{profile.link}">власному сліді</a>!
                        </p>
                        <p>Звертаємо Вашу увагу, що інофрмація буде опублікована лише після модераціі.</p>',
        ]);
        DB::table('texts')->insert([
            'id' => 3,
            'slug' => 'reserve-tracks',
            'desc' => 'Користувач забронював сліди',
            'text' => 'Дякуемо, сліди заброньовані на 60 хвилин!',
        ]);
        DB::table('texts')->insert([
            'id' => 4,
            'slug' => 'unreserve-track',
            'desc' => 'Користувач видалив слід з броні',
            'text' => 'Слід видалено з броні!',
        ]);
    }
}
