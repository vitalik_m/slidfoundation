<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UsersTableSeeder::class);
        $this->call(PagesTableSeeder::class);
        $this->call(TextsTableSeeder::class);
        $this->call(DatasTableSeeder::class);

        if(env('APP_ENV') == 'local') {
            $this->call(TracksTableSeeder::class);
            $this->call(ReservesTableSeeder::class);
            $this->call(NewArticlesTableSeeder::class);
            $this->call(ShortNewsTableSeeder::class);
        }
    }
}
