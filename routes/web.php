<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
//
use App\Events\WebsocketDemoEvent;
use App\Http\Controllers\HomeController;

Route::prefix('admin')->middleware('auth:web')->middleware('canAdmin')->group( function () {
    Route::get('/', 'admin\TrackController@index')->name('admin');
    Route::resource('track', 'admin\TrackController');
    Route::post('track/bulkStore', 'admin\TrackController@bulkStore')->name('track.bulkStore');
    Route::post('track/setCurrent', 'admin\TrackController@setCurrent')->name('track.setCurrent');

    Route::resource('page', 'admin\PageController')->only(['edit', 'update', 'index']);
    Route::resource('text', 'admin\TextController')->except(['destroy']);
    Route::resource('newArticle', 'admin\NewArticleController')->except(['show']);

    Route::resource('user', 'admin\UserController')->only(['index']);
    Route::patch('user/{user}/ban', 'admin\UserController@ban')->name('user.ban');
    Route::patch('user/{user}/un-ban', 'admin\UserController@unBan')->name('user.un-ban');

    Route::resource('message', 'admin\MessageController')->except(['show', 'create']);

    Route::resource('message', 'admin\MessageController')->except(['show']);
    Route::resource('shortNew', 'admin\ShortNewController')->except(['show']);
});

Auth::routes();
Route::get('auth/facebook', 'Auth\AuthController@loginFacebook')->name('auth-facebook');
Route::get('auth/facebook/callback', 'Auth\AuthController@userFromFacebook');
Route::get('auth/google', 'Auth\AuthController@loginGoogle')->name('auth-google');
Route::get('auth/google/callback', 'Auth\AuthController@userFromGoogle');

Route::get('data/update-tracks', 'DataController@updateTracksData')
//    ->middleware('auth:web')->middleware('canAdmin')
    ->name('update-data-tracks');

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/user/{id}/profile', 'UserController@profile')->name('user.profile');
Route::get('/home/ajax-track-popup/{track}', 'HomeController@ajaxTrackPopup')->name('home.ajax-track-popup');
Route::get('/home/ajax-track-popup/', 'HomeController@ajaxTrackPopup')->name('home.ajax-track-popup2');
Route::get('/home/ajax-tracks-by-bounds/', 'HomeController@ajaxGetTracks')->name('home.ajax-tracks-by-bounds');
Route::post('/home/ajax-update-position/', 'HomeController@ajaxUpdateCurrentPosition')
    ->middleware('auth:web')
    ->middleware('isTracking')
    ->name('home.ajax-update-position');
Route::get('/page/{slug}/', 'HomeController@page')->name('page');
Route::get('/rating/', 'HomeController@rating')->name('rating');

Route::get('/news/', 'HomeController@news')->name('news');
Route::get('/new/{id}', 'HomeController@singleNew')->name('single-new');

Route::prefix('user')->middleware('auth:web')->group( function () {
    Route::patch('{id}/update', 'UserController@update')->name('user.update');
    Route::patch('{id}/ajax-update', 'UserController@ajaxUpdate')->name('user.ajax-update');
    Route::patch('{user}/track-update/{track}', 'UserController@trackUpdate')->name('user.track-update');
});

Route::prefix('order')->middleware('auth:web')->group( function () {
    Route::patch('track/{track}/reserve', 'OrderController@reserve')->name('track.reserve');
    Route::patch('track/{track}/ajax-reserve', 'OrderController@ajaxReserve')->name('track.ajax-reserve');
    Route::patch('track/ajax-unreserve/', 'OrderController@ajaxUnReserve')->name('track.ajax-unreserve');
    Route::post('ajax-create', 'OrderController@ajaxCreate')->name('order.ajax-create');
    Route::post('ajax-approve', 'OrderController@ajaxApprove')->name('order.ajax-approve');
    Route::post('ajax-pending', 'OrderController@ajaxPending')->name('order.ajax-pending');
    Route::post('ajax-decline', 'OrderController@ajaxDecline')->name('order.ajax-decline');
});

//Route::get('/chats', 'ChatController@index');
Route::get('/messages', 'ChatController@fetchMessages');
Route::middleware('auth:web')->middleware('isHasTracks')->middleware('isChatBlocked')->post('/messages', 'ChatController@sendMessage');