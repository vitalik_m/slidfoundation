<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Text extends Model
{
    public $table = 'texts';

    public $fillable = ['slug', 'text', 'desc'];

    public static function bySlug($slug)
    {
        return self::where('slug',$slug)->first();
    }

    public static function bySlugs(array $slugs)
    {
        return self::whereIn('slug',$slugs)->get()->pluck('text', 'slug');
    }
}
