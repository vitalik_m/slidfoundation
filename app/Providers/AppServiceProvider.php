<?php

namespace App\Providers;

use App\Helpers\SideMenuData;
use App\ShortNew;
use App\Text;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        require_once app_path().'/Helpers/SideMenuData.php';
        require_once app_path().'/Text.php';
        require_once app_path().'/ShortNew.php';
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::share('sideMenuData', SideMenuData::allData());
        View::share('text', Text::all()->pluck('text','slug'));
        View::share('lastShortNews', ShortNew::where('id','>','0')->orderBy('created_at', 'desc')->take(15)->get());
    }
}
