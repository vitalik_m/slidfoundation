<?php
/**
 * Created by PhpStorm.
 * User: rakkot
 * Date: 6/25/2019
 * Time: 10:00 PM
 */

namespace App\Http\Controllers\Auth;

use App\User;
use Laravel\Socialite\Facades\Socialite;
use Stevebauman\Location\Facades\Location;

class AuthController
{
    public function loginFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function userFromFacebook()
    {
        try {
            $getInfo = Socialite::driver('facebook')->user();
        } catch (\Exception $e) {
            return redirect('/login')->with('error', 'Помилка авторизації');
        }

        $user = $this->createUser($getInfo,'facebook');
        auth()->login($user);
        return redirect()->to('/home');
    }

    public function loginGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function userFromGoogle()
    {
        try {
            $getInfo = Socialite::driver('google')->user();
        } catch (\Exception $e) {
            return redirect('/login')->with('error', 'Помилка авторизації');
        }

        $user = $this->createUser($getInfo,'google');
        auth()->login($user);
        return redirect()->to('/home');
    }

    function createUser($getInfo,$provider){
        $user = User::where('email', $getInfo->email)->first();
        if (!$user) {
            $user = User::create([
                'name'     => $getInfo->name,
                'email'    => $getInfo->email,
                'photo'    => $getInfo->avatar,
                'provider' => $provider,
                'provider_id' => $getInfo->id,
                'countryCod' => Location::get() ? Location::get(request()->ip())->countryCode : '',
            ]);
        }
        return $user;
    }
}