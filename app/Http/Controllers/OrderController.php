<?php

namespace App\Http\Controllers;

use App\Helpers\WayForPay;
use App\Order;
use App\Reserve;
use App\Text;
use App\Track;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function ajaxReserve(Request $request, Track $track)
    {
        $reserve = $request->reserve ? Reserve::find($request->reserve) : new Reserve() ;

        if(!$track->exists) return response()->json(['status' => 'error', 'msg' => 'Слід не знайдено']);
        if(!$track->amount) return response()->json(['status' => 'error', 'msg' => 'Слід повинен мати ціну']);
        if($track->isReserved()) return response()->json(['status' => 'error', 'msg' => 'Слід вже заброньовано іншим користувачем']);

        $track->reserve_id = $this->createReserve($reserve, Auth::id());
        $track->save();

        return response()->json(['status' => 'ok', 'msg' => Text::bySlug('reserve-tracks')->text, 'reserv_id' => $track->reserve_id]);
    }

    public function ajaxUnReserve(Request $request, Track $track)
    {
        $track = Track::find($request->track);
        $reserve = Reserve::find($request->reserve_id);

        if(!$track) return response()->json(['status' => 'error', 'msg' => 'Слід не знайдено']);
        if($track->reserve_id != $reserve->id) return  response()->json(['status' => 'error', 'msg' => '']);

        $track->reserve_id = null;
        $track->save();

        $reserve->delIfEmpty();

        return response()->json(['status' => 'ok', 'msg' => Text::bySlug('unreserve-track')->text]);
    }

    public function ajaxCreate(Request $request)
    {
        if(!$request->reserve_id) return response()->json(['status' => 'error', 'message' => 'Треба вказати ИД броні'])->status(418);
        $reserve = Reserve::findOrFail($request->reserve_id);
        if(!$reserve->isActual()) return response()->json(['status' => 'error', 'message' => 'Час вашої броні вичерпано'])->status(418);
        $order = new Order();
        $order->user_id = Auth::id();
        $order->save();

        $reserve->order_id = $order->id;
        $reserve->save();

        $data = (new WayForPay($reserve, $order))->allData();

        return $data;
    }

    public function ajaxApprove(Request $request)
    {
        if(!$request->reserve_id) return response()->json(['status' => 'error', 'message' => 'Треба вказати ИД броні'])->status(418);
        $reserve = Reserve::findOrFail($request->reserve_id);

        $status = $reserve->order->checkStatus();
        if($status == Order::APPROVE){
            $reserve->order->approve();

            (new DataController())->updateTracksData();

            return response()->json(['status' => 'ok', 'message' => $this->msgOk()]);
        }

        return response()->json(['status' => 'error', 'message' => 'Щось не так'])->status(418);
    }

    public function ajaxPending(Request $request)
    {
        if(!$request->reserve_id) return response()->json(['status' => 'error', 'message' => 'Треба вказати ИД броні'])->status(418);
        $reserve = Reserve::findOrFail($request->reserve_id);

        $reserve->order->pending();

        $text = '';

        return response()->json(['status' => 'ok', 'message' => $text]);
    }

    public function ajaxDecline(Request $request)
    {
        if(!$request->reserve_id) return response()->json(['status' => 'error', 'message' => 'Треба вказати ИД броні'])->status(418);
        $reserve = Reserve::findOrFail($request->reserve_id);

        $reserve->order_id = null;
        $reserve->save();
    }

    public function deleteReserve(Request $request, Track $track)
    {
        $user = $request->user ? User::find($request->user) : null ;

        if(!$track->exists) return response()->json(['error', 'Слід не знайдено']);
        if(!$user->exists) return response()->json(['error', 'Користувача не знайдено']);

        $track->reserve_id = null;
        $track->save();

        return response()->json(['success', Text::bySlug('unreserve-track')->text]);
    }

    private function createReserve($reserve, $user_id)
    {
        if(!$reserve){
            $reserve = new Reserve();
        }

        if(!$reserve->exists){
            $reserve->user_id = $user_id;
            $reserve->save();
        }

        return $reserve->id;
    }

    private function msgOk(){
        return str_replace('{profile.link}', route('user.profile', ['id' => \Auth::id()]), Text::bySlug('buy-tracks')->text);
    }
}
