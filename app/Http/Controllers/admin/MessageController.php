<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Message;

class MessageController extends Controller
{
    public function index()
    {
        $messages = Message::all();

        return view('admin.message.index', compact('messages'));
    }

    public function edit(Message $message)
    {
        return view('admin.message.edit', compact('message'));
    }

    public function update(Request $request, Message $message)
    {
        $message->fill($request->all());

        if($message->save()){
            return redirect(route('message.index'))->with('success', 'Повідомлення оновлено!');
        }

        return back()->with('error', 'Щось не так, Повідомлення не оновлено!');
    }

    public function destroy(Message $message)
    {
        if($message->delete()){
            return redirect(route('message.index'))->with('success', 'Повідомлення видалено!');
        }

        return back()->with('error', 'Щось не так, Повідомлення не видалено!');
    }
}
