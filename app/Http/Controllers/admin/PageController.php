<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{

    public function index()
    {
        $pages = Page::all();

        return view('admin.page.index', compact('pages'));
    }

    public function edit(Page $page)
    {
        return view('admin.page.edit', compact('page'));
    }

    public function update(Request $request, Page $page)
    {
        if($page->update($request->all())){
            return back()->with('success', 'Сторінка оновлена!');
        }

        return back()->with('error', 'Щось не так, сторінка не оновлена');
    }
}
