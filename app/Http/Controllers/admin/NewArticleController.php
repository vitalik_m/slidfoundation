<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\NewArticle;
use Illuminate\Http\Request;

class NewArticleController extends Controller
{
    public function index()
    {
        $newArticles = NewArticle::all();

        return view('admin.newArticle.index', compact('newArticles'));
    }

    public function create()
    {
        $newArticle = new NewArticle();

        return view('admin.newArticle.create', compact('newArticle'));
    }

    public function store(Request $request)
    {
        $newArticle = (new NewArticle())->fill($request->all());

        if($newArticle->save()){
            return redirect(route('newArticle.index'))->with('success', 'Новину створено!');
        }

        return back()->with('error', 'Щось не так, Новина не створена!');
    }

    public function edit(NewArticle $newArticle)
    {
        return view('admin.newArticle.edit', compact('newArticle'));
    }

    public function update(Request $request, NewArticle $newArticle)
    {
        if($newArticle->update($request->all())){
            return redirect(route('newArticle.index'))->with('success', 'Новину оновлено!');
        }

        return back()->with('error', 'Щось не так, Новина не оновлена!');
    }

    public function destroy(NewArticle $newArticle)
    {
        if($newArticle->delete()){
            return redirect(route('newArticle.index'))->with('success', 'Новину видалено!');
        }

        return back()->with('error', 'Щось не так, Новина не видалена!');
    }
}
