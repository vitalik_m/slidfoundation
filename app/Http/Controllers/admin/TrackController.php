<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Track;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TrackController extends Controller
{
    protected $trackPerPage = 500;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!$request->page) {
            $all = Track::where('isCurrent', '<=', '1')->pluck('isCurrent');
            $posCurent = array_search('1', $all->toArray());
            $page = ceil($posCurent / $this->trackPerPage);
            $tracks = Track::with('owner')->paginate( $this->trackPerPage, ['*'], 'page', $page);
        }else{
            $tracks = Track::with('owner')->paginate( $this->trackPerPage);
        }

        return view('admin.track.index', compact('tracks'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function bulkStore(Request $request)
    {
        if(!$request->tracksfile){
            return back()->with('success', 'Треба додати файл зі слідами');
        }

        $path = $request->tracksfile->path();
        $tracks = simplexml_load_file($path)->trk->trkseg->trkpt;
        foreach ($tracks as $key => $track){
            $model = new Track();
            foreach ($track->attributes() as $key => $value) {
                if($key == 'lat' || $key == 'lon'){
                    $model->$key = $value->__toString();
                }
            }
            $model->save();
        }

        return back()->with('success', 'Cліди додані!');
    }

    public function setCurrent(Request $request)
    {
        $oldCurrent = Track::currentPosition();

        if($oldCurrent && $oldCurrent->id == $request->trackId){
            return back()->with('default', 'Ви залишаєтесь на томуж місці!');
        }

        if(Track::setCurrent($request->trackId)) {

            if($oldCurrent) {
                $oldCurrent->isCurrent = 0;
                $oldCurrent->save();
            }
            return back()->with('success', 'Ви перемістилися! :)');
        }

        return back()->with('error', 'Щось нетак :(');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\track  $track
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $track = Track::findOrFail($id);

        return view('admin.track.edit', compact('track'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\track  $track
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Track $track)
    {
        $track->status = $request->status;

        if($request->file()){
            $track->img = $request->img->store($track::IMG_PATH);
        }

        if($track->update($request->all())){
            return back()->with('success', 'Cлід оновлено!');
        }

        return back()->with('error', 'Помилка оновлення сліду!');
    }
}
