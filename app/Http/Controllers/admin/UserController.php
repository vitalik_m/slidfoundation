<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();

        return view('admin.user.index', compact('users'));
    }

    public function ban(User $user)
    {
        $user->is_ban = 1;
        if($user->save()){
            return back()->with('success', 'Користувача заблоковано!');
        }

        return back()->with('error', 'Щось не так, Користувача не заблоковано!');
    }

    public function unBan(User $user)
    {
        $user->is_ban = 0;
        if($user->save()){
            return back()->with('success', 'Користувача розблоковано!');
        }

        return back()->with('error', 'Щось не так..!');
    }
}
