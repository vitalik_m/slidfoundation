<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\ShortNew;
use Illuminate\Http\Request;

class ShortNewController extends Controller
{
    public function index()
    {
        $shortNews = ShortNew::all();

        return view('admin.shortNew.index', compact('shortNews'));
    }

    public function create()
    {
        $shortNew = new ShortNew();

        return view('admin.shortNew.create', compact('shortNew'));
    }

    public function store(Request $request)
    {
        $shortNew = (new ShortNew())->fill($request->all());

        if($shortNew->save()){
            return redirect(route('shortNew.index'))->with('success', 'Новину створено!');
        }

        return back()->with('error', 'Щось не так, Новина не створена!');
    }

    public function edit(ShortNew $shortNew)
    {
        return view('admin.shortNew.edit', compact('shortNew'));
    }

    public function update(Request $request, ShortNew $shortNew)
    {
        if($shortNew->update($request->all())){
            return redirect(route('shortNew.index'))->with('success', 'Новину оновлено!');
        }

        return back()->with('error', 'Щось не так, Новина не оновлена!');
    }

    public function destroy(ShortNew $shortNew)
    {
        if($shortNew->delete()){
            return redirect(route('shortNew.index'))->with('success', 'Новину видалено!');
        }

        return back()->with('error', 'Щось не так, Новина не видалена!');
    }
}
