<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\TextCreateRequest;
use App\Text;
use Illuminate\Http\Request;

class TextController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $texts = Text::all();
        return view('admin.text.index', compact('texts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $text = new Text();
        return view('admin.text.create', compact('text'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TextCreateRequest $request)
    {
        $text = new Text();
        $text->fill($request->all());

        if($text->save()){
            return redirect(route('text.index'))->with('success', 'Текст створено!');
        }

        return back()->with('error', 'Щось не так, текст не створено!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Text $text)
    {
        return view('admin.text.edit', compact('text'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Text $text)
    {
        if($text->update($request->all())){
            return back()->with('success', 'Текст оновлено!');
        }

        return back()->with('error', 'Щось не так, текст не оновлено');
    }
}
