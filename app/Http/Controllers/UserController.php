<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserUpdateRequest;
use App\Track;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Stevebauman\Location\Facades\Location;

class UserController extends Controller
{
    public $tracksPerPage = 10;

    public function profile($id)
    {
        $user = User::find($id);

        $tracks = null ;
        if($user->trackIsPublic || $user->isOwner()) {
            $page = request()->page ? request()->page: 1;
            $tracks = Track::where('user_id', $user->id)->paginate($this->tracksPerPage, ['*'], 'page', $page);
        }


        return view('profile', compact('user', 'tracks'));
    }

    public function update(UserUpdateRequest $request, $id)
    {
        if($id != Auth::id()){
            return back()->with('error', 'Ви повинні бути власником профілю');
        }

        if($user = User::find($id)){
            $user->fill($request->all());

            if($request->file()){
                $user->photo = $request->photo->store('images/users/');
            }
            if($request->file()){
                $user->photo = $request->photo->store('images/users/');
            }

            $user->save();
            return back()->with('success', 'Профіль оновлено');
        }

        return back()->with('error', 'Такого користувача немає');
    }

    public function ajaxUpdate(UserUpdateRequest $request, $id)
    {
        if($id != Auth::id()){
            return response()->json(['status' => 'error', 'msg' => 'Ви повинні бути власником профілю']);
        }

        if($user = User::find($id)){
            $user->fill($request->all());

            if($request->file()){
                $user->photo = $request->photo->store('images/users/');
            }

            $user->trackIsPublic = $request->trackIsPublic ? 1 : 0 ;

            $user->save();
            return response()->json(['status' => 'success', 'msg' => 'Профіль оновлено']);
        }

        return back()->with(['error' => 'error', 'msg' => 'Такого користувача немає']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\track  $track
     * @return \Illuminate\Http\Response
     */
    public function trackUpdate(Request $request, User $user, Track $track)
    {
        if(!$user->isOwner()) return back()->with('error', 'Ви повинні бути власником профілю!');
        if($track->user_id != $user->id) return back()->with('error', 'Ви повинні бути власником сліда!');

        $track->status = $track->statuses['pending'];
        $track->fill($request->all());

        if($request->file()){
            $track->img = $request->img->store($track::IMG_PATH);
        }
        if($track->video){
            preg_match('/[a-zA-Z0-9_-]+$/', $track->video, $matches);
            $track->video = 'https://www.youtube.com/embed/' . $matches[0];
        }

        if($track->save() ){
            return back()->with('success', 'Cлід оновлено!');
        }

        return back()->with('error', 'Помилка оновлення сліду!');
    }
}
