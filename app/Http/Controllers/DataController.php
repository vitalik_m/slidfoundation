<?php

namespace App\Http\Controllers;


use App\Data;
use App\Track;

class DataController extends Controller
{
    public function updateTracksData()
    {
        $free = Data::where('title', 'tracks_free')->first();
        $free->content = Track::where('user_id', '1')->count();
        $free->save();

        $bought = Data::where('title', 'tracks_bought')->first();
        $bought->content = Track::where('user_id', '>', '1')->count();
        $bought->save();

        $all = Data::where('title', 'tracks_all')->first();
        $all->content = $bought->content + $free->content;
        $all->save();

        return redirect(route('home'));
    }
}
