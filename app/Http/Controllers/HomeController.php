<?php

namespace App\Http\Controllers;

use App\NewArticle;
use App\Page;
use App\Position;
use App\ShortNew;
use App\Text;
use App\Track;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Stevebauman\Location\Facades\Location;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $curPosition = Track::currentPosition();

        if($curPosition){
            $tracks = $curPosition->sibilings(Track::countTrackPerQuery());
        }else{
            $tracks = Track::take(Track::countTrackPerQuery())->get();
            $tracks = $tracks ? $tracks : [] ;
        }

        if($curPosition){
            $curPosition = $curPosition->toArray();
        }else{
            $curPosition = $tracks->isEmpty() ? [ 'lat' => 59.91273, 'lon' => 10.74609] : $tracks[0] ;
        }

        $center = $this->center($curPosition);

        $texts = Text::bySlugs(['cart_closing_notif']);
        $currentInfo = [
            'day' => Carbon::createFromFormat('d-m-y', env('START_TRAVEL_DATE'))->diffInDays(Carbon::now()),
            'time' => Carbon::now('Europe/Oslo')->format('H:i'),
            'place' => $curPosition ? '#'.$curPosition['id'] .' '. $curPosition['title'] : ''];

        $tracks = json_encode($tracks->keyBy('id'));
        return view('home', compact('curPosition', 'tracks', 'center', 'currentInfo', 'texts'));
    }

    public function ajaxGetTracks()
    {
        $curPosition = Track::currentPosition();

        $tracks = $curPosition->sibilings();

        return response()->json(['tracks' => $tracks->keyBy('id'), 'current_position' => $curPosition]);
    }

    public function ajaxTrackPopup(Track $track)
    {
        if(!$track || !$track->exists) return response()->json(['status' => 'error', 'msg' => 'Слід не знайдено!']);

        if($track->isInPaymentProcess()) $track->reserve->order->checkStatus();

        $template = $track->isBought() ? 'popups.bought_track' : 'popups.track' ;

        $track->setLastTrackContent();

        $reserve_id = Auth::user() ? Auth::user()->getActiveReserve() : '';
        $texts = Text::bySlugs(['popup_track_free', 'popup_track_reserved']);

        $data = view($template, compact('track', 'reserve_id', 'texts'))->render();

        return response()->json(['status' => 'success', 'data' => $data]);
    }

    public function ajaxUpdateCurrentPosition(Request $request)
    {
        if(!$request->position) return false;

        (new Position())->fill($request->position)->save();

        $nearestTrackId = Track::nearestBy($request->position);

        $oldCurrent = Track::currentPosition();

        if($oldCurrent && $oldCurrent->id == $nearestTrackId){
            return response()->json(['status'=>'no changes', 'msg' => 'Ви залишаєтесь на томуж місці!']);
        }

        if(Track::setCurrent($nearestTrackId)) {
            if($oldCurrent) {
                $oldCurrent->isCurrent = 0;
                $oldCurrent->save();
            }
        }

        return response()->json(['status'=>'ok', 'msg' => 'Ви перемістились']);
    }

    private function center($curPosition)
    {
        if(request()->lat && request()->lon){
            $center = ['lat' => request()->lat, 'lon' => request()->lon];
        }else{
            $center = $curPosition ? $curPosition : Track::DEFAULT_POSITION ;
        }
        return $center;
    }

    public function page($slug)
    {
        $page = Page::where('slug', $slug)->first();

        return view('page', compact('page'));
    }

    public function news()
    {
        $news = NewArticle::where('id','>','0')->orderBy('created_at', 'desc')->take(20)->get();
        $shortNews = ShortNew::where('id','>','0')->orderBy('created_at', 'desc')->take(30)->get();

        $month = ['Січень','Лютий','Березень','Квітень','Травень','Червень','Липень','Серпень','Вересень',' Жовтень','Листопад','Грудень'];

        return view('news', compact('news', 'shortNews','month'));
    }

    public function singleNew($id)
    {
        $newArticle = NewArticle::findOrFail($id);

        return view('new-single', compact('newArticle'));
    }

    public function rating()
    {
        $users = User::where('id', '>', '1')->orderBy('tracks_count', 'desc')->paginate(25);

        return view('rating', compact('users'));
    }
}
