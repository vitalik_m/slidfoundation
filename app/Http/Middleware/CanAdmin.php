<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CanAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::user()){
            return redirect(route('login'))->with('error', 'Треба авторизуватися');
        }elseif(!Auth::user()->canAdmin()){
            return redirect(route('home'))->with('error', 'Вам недостатньо прав');
        }else{
            return $next($request);
        }
    }
}
