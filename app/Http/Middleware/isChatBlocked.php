<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class isChatBlocked
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->is_ban){
            return response()->json(['status' => 'error', 'msg' => 'Вам заборонено писати в чат'], 403);
        }else{
            return $next($request);
        }
    }
}
