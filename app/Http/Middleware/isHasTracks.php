<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class isHasTracks
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::user()){
            return redirect(route('login'))->with('error', 'Треба авторизуватися');
        }elseif(!Auth::user()->tracks_count){
            return response()->json(['msg' => 'Вам треба придбаті хочаб один слід!'], 418);
        }else{
            return $next($request);
        }
    }
}
