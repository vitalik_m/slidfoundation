<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TextCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug' => 'required|regex:/^[a-zA-Z-_]+$/u|unique:texts',
            'text' => 'required|string',
            'desc' => 'required|string',
        ];
    }
}
