<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravelista\Comments\Commentable;

class NewArticle extends Model
{
    use SoftDeletes, Commentable;

    public $table = 'new_articles';

    public $fillable = ['title', 'content'];
}
