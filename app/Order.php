<?php

namespace App;

use App\Helpers\WayForPay;
use App\Http\Controllers\DataController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Order extends Model
{
    const PENDING = 'pending';
    const APPROVE = 'Approved';
    const DECLINE = 'Declined';
    const NEW = 'new';

    public function reserve()
    {
        return $this->hasOne('App\Reserve', 'order_id', 'id');
    }

    public function approve()
    {
        $this->status = self::APPROVE;
        $this->save();

        foreach($this->reserve->tracks as $track){
            $track->user_id = $this->user_id;
            $track->order_id = $this->id;
            $track->save();

            User::find($this->user_id)->updateTracksCount();
        }
        (new DataController())->updateTracksData();
    }

    public function pending()
    {
        $this->status = self::PENDING;
        $this->save();
    }

    public function deny()
    {
        $this->status = self::DECLINE;
        $this->save();

        foreach($this->reserve->tracks as $track){
            $track->user_id = User::ADMIN_ID;
            $track->reserve_id = null;
            $track->save();
        }
        (new DataController())->updateTracksData();
    }

    public function checkStatus()
    {
        $status = (new WayForPay('', $this))->checkStatus();

        if($status == 'InProcessing' || $status == 'Pending' || $status == 'WaitingAuthComplete'){
            $this->pending();
        }elseif ($status == 'Approved' && $this->status){
            $this->approve();
        }else{
            $this->deny();
        }

        return $status;
    }
}
