<?php
/**
 * Created by PhpStorm.
 * User: rakkot
 * Date: 6/8/2019
 * Time: 9:26 PM
 */

namespace App\Services;


use App\Track;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TracksByDistances
{
    public $tracks = [];

    private $distances;

    private $distancesAdmin = [['100', '2'], ['1000', '20']];
//    private $distancesAll = [['100', '0'], ['1000', '5'], ['5000', '15'], ['', '30']];
    private $distancesAll = [['100', '0'], ['500', '5'], ['1000', '15'], ['3000', '20'], ['5000', '35'], ['', '50']];

    private $model;
    private $lastKey;
    private $firstKey;
    private $direct = '+';

    public function __construct($model)
    {
        $this->distances = Auth::user() && Auth::user()->isTracking() ? $this->distancesAdmin :  $this->distancesAll ;

        $this->model = $model;
        $this->lastKey = (Track::select('id')->orderby('id', 'desc')->first())->id;
        $this->firstKey = (Track::select('id')->orderby('id', 'asc')->first())->id;
        $this->tracks = collect([]);

        $this->makeTracksList();
    }

    private function makeTracksList()
    {
        $this->passDistances();
        $this->direct = '-';
        $this->passDistances();

        $this->plusBoughtTracks();
        $this->plusCenterTrack();

        $this->tracks
            ->sortKeys();
    }

    private function passDistances()
    {
        foreach ($this->distances as $index => $part) {
            $from = $this->getFrom($index);
            $to = $this->getTo($part);

            $operantFrom = $this->getOperandFrom($index);
            $whereTo = $this->getWhereTo($to);
            $mod = $part[1] ? ' and MOD(id,' . $part[1] . ') = 0' : '';

            $tracks = Track::select('id', 'lon', 'lat', 'title', 'user_id', 'isCurrent', 'reserve_id')
                ->whereRaw('`id` ' . $operantFrom . ' ' . $from . ' ' . $whereTo . $mod . ' and `user_id` = 1')
                ->get();

            $this->mergeTracks($tracks);

            if ($this->lastKey == $to) break;
            if ($this->firstKey == $to) break;
        }

        return;
    }

    private function getFrom($index)
    {
        if ($this->direct == '+') {
            $from = $index ? $this->model->id + $this->distances[$index - 1][0] : $this->model->id;
            return $from > $this->lastKey ? $this - lastKey : $from;
        } else {
            $from = $index ? $this->model->id - $this->distances[$index - 1][0] : $this->model->id;
            return $from < $this->firstKey ? $this->firstKey : $from;
        }
    }

    private function getTo($part)
    {
        if ($this->direct == '+') {
            if (!$part[0]) return $this->lastKey;

            $toKey = $this->model->id + $part[0];
            return $toKey < $this->lastKey ? $toKey : $this->lastKey;
        } else {
            if (!$part[0]) return $this->firstKey;

            $toKey = $this->model->id - $part[0];
            return $toKey < $this->firstKey ? $this->firstKey : $toKey;
        }
    }

    private function mergeTracks($tracks)
    {
        $this->tracks = $this->tracks->merge($tracks);
    }

    private function plusBoughtTracks()
    {
        $tracks = Track::select('id', 'lon', 'lat', 'title', 'user_id', 'isCurrent', 'reserve_id')
            ->where('user_id', '>', '1')
            ->get();

        $this->tracks = $this->tracks->merge($tracks);
    }

    private function getOperandFrom($index)
    {
        if ($this->direct == '+') {
            return $index ? '>' : '>=';
        } else {
            return $index ? '<' : '<=';
        }
    }

    private function getWhereTo($to)
    {
        if ($this->direct == '+') {
            return $to ? ' and `id` <= ' . $to : ' ';
        } else {
            return $to ? ' and `id` >= ' . $to : $this->firstKey;
        }
    }

    private function plusCenterTrack()
    {
        if(!request('lon') || !request('lat')) return;

        $track = Track::select('id', 'lon', 'lat', 'title', 'user_id', 'isCurrent', 'reserve_id')
            ->where('lon', request('lon'))
            ->where('lat', request('lat'))
            ->get();

        $this->tracks = $this->tracks->merge($track);
    }
}
