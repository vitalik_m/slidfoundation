<?php
/**
 * Created by PhpStorm.
 * User: rakkot
 * Date: 7/23/2019
 * Time: 10:59 PM
 */

namespace App\Services;


use App\Track;

/**
 * @property  countShowingTracks
 */
class TracksByPosition
{
    const DISTANCES_FROM_ZOOM = [
        '50' => 0, '15' => 2, '14' => 6,
        '13' => 10, '12' => 25, '11' => 50,
        '10' => 100, '9' => 250, '8' => 500,
        '7' => 1000, '6' => 2500, '5' => 3000,
        '4' => 4000
    ];
    const COUNT_SHOWING_TRACKS = 500;
    const COUNT_TRACKS_OUT_WINDOW = 100;

    public $tracks = [];

    private $cordinates;
    private $zoom;
    private $centerTrack;
    private $distance;

    private $model;
    private $lastTrack;

    public function __construct($model)
    {
        $this->model = $model;
        $this->tracks = collect([]);

        $this->setCordinates();
        $this->setZoom();

        $this->setDistance();
        $this->setCenterTrack();

        $this->setLastTrack();

        if($this->cordinates){
            $this->makeTracksListByLatLon();
        }else{
            $this->makeTracksList();
        }

    }

    private function makeTracksList()
    {
        $idTrackFrom = $this->getFrom();
        $idTrackTo = $this->getTo();

        $mod = $this->distance ? ' and MOD(id,' . $this->distance . ') = 0' : '';

        $tracks = Track::select('id', 'lon', 'lat', 'title', 'user_id', 'isCurrent', 'reserve_id')
            ->whereRaw('`id` > ' . $idTrackFrom . ' and `id` < ' . $idTrackTo . $mod)
            ->get();

        $this->mergeTracks($tracks);

        return;
    }

    private function makeTracksListByLatLon()
    {
        $mod = $this->distance ? ' and MOD(id,' . $this->distance . ') = 0' : '';

        $lon = ' `lon` > '. $this->cordinates[0]['lon'] .' AND `lon` < '. $this->cordinates[1]['lon'];
        $lat = ' AND `lat` > '. $this->cordinates[0]['lat'] . ' AND `lat` < '. $this->cordinates[1]['lat'];

        $tracks = Track::select('id', 'lon', 'lat', 'title', 'user_id', 'isCurrent', 'reserve_id')
            ->whereRaw($lon . $lat . $mod)
            ->get();

        $this->mergeTracks($tracks);

//        $this->mergeOutWindowTracks();

        return;
    }

    private function getFrom()
    {
        if ($this->cordinates) {
            $trackFrom = Track::where('lat', '<', $this->cordinates[0]['lat'])
                ->where('lon', '<', $this->cordinates[0]['lon'])
                ->latest('id')
                ->first();

            $idTrackFrom = $trackFrom->id - self::COUNT_TRACKS_OUT_WINDOW;
        } else {
            $idTrackFrom = $this->centerTrack->id - self::COUNT_SHOWING_TRACKS;
        }
        $idTrackFrom = $idTrackFrom > 0 ? $idTrackFrom : 0;

        return $idTrackFrom;
    }

    private function getTo()
    {
        if ($this->cordinates) {
            $trackTo = Track::where('lat', '>', $this->cordinates[0]['lat'])
                ->where('lon', '>', $this->cordinates[0]['lon'])
                ->first();

            $idTrackTo = $trackTo->id + self::COUNT_TRACKS_OUT_WINDOW;

        } else {
            $idTrackTo = $this->centerTrack->id + self::COUNT_SHOWING_TRACKS;
        }
        $idTrackTo = $idTrackTo > $this->lastTrack->id ? $this->lastTrack->id : $idTrackTo;

        return $idTrackTo;
    }

    private function mergeTracks($tracks)
    {
        $this->tracks = $this->tracks->merge($tracks);
    }

    private function plusBoughtTracks()
    {
        $tracks = Track::select('id', 'lon', 'lat', 'title', 'user_id', 'isCurrent', 'reserve_id')
            ->where('user_id', '>', '1')
            ->get();

        $this->tracks = $this->tracks->merge($tracks);
    }

    private function setCenterTrack()
    {
        if(!request('lon') || !request('lat')){
            $track = $this->getTrackByCurrentPosition();
        }else{
            $track = $this->getTrackByLatLon();
            if(!$track){
                $track = $this->getTrackByCurrentPosition();
                //->with('error', 'Помилка: сліду з такими кординатами не існує');
            }
        }

        if($this->distance && $track->id % $this->distance){
            $this->tracks = $this->tracks->merge(collect([$track]));
        }

        $this->centerTrack = $track;
    }

    private function setCordinates()
    {
        if(request('cordinates')){
            $this->cordinates = [
                [
                    'lat' => request('cordinates')[0]['lat'],
                    'lon' => request('cordinates')[0]['lon']
                ],
                [
                    'lat' => request('cordinates')[1]['lat'],
                    'lon' => request('cordinates')[1]['lon']
                ]
            ];
        }else{
            $this->cordinates = false;
        }
    }

    private function setZoom()
    {
        if(request('zoom')){
            $this->zoom = request('zoom');
        }else{
            $this->zoom = false;
        }
    }

    private function setLastTrack()
    {
        $this->lastTrack = Track::latest('id')->first();
    }

    private function setDistance()
    {
        if($this->zoom){
            foreach(self::DISTANCES_FROM_ZOOM as $key => $currDistance){
                if( $key >= $this->zoom){
                    $distance = $currDistance;
                }else{
                    break;
                }
            }
        }else{
            $distance = 0;
        }

        $this->distance = isset($distance) ? $distance : 0;
    }

    private function getTrackByLatLon()
    {
        return Track::select('id', 'lon', 'lat', 'title', 'user_id', 'isCurrent', 'reserve_id')
            ->where('lon', request('lon'))
            ->where('lat', request('lat'))
            ->first();
    }

    private function getTrackByCurrentPosition()
    {
        return Track::select('id', 'lon', 'lat', 'title', 'user_id', 'isCurrent', 'reserve_id')
            ->where('isCurrent', '1')
            ->first();
    }

    private function mergeOutWindowTracks()
    {

    }
}