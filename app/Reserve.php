<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Reserve extends Model
{
    const RESERVE_TIME = 1;

    public function tracks()
    {
        return $this->hasMany('App\Track', 'reserve_id', 'id');
    }

    public function order()
    {
        return $this->hasOne('App\Order', 'id', 'order_id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function isActual()
    {
        if($this->order_id && $this->order->status != Order::DECLINE) {
            $this->order->checkStatus();
        }

        if($this->order_id && $this->order->status != Order::DECLINE){
            return 'pending';
        }elseif($this->updated_at > Carbon::now()->subHours(self::RESERVE_TIME)){
            return 'time';
        }

        return false;
    }

    public function delIfEmpty()
    {
        if($this->tracks->isEmpty()){
            $this->delete();
        }
    }
}
