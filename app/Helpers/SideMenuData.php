<?php
/**
 * Created by PhpStorm.
 * User: rakkot
 * Date: 6/8/2019
 * Time: 9:26 PM
 */

namespace App\Helpers;


use App\Data;
use App\User;

class SideMenuData
{
    public static function allData()
    {
        return [
            'tracks' => self::tracks(),
            'users' => self::users()
        ];
    }

    public static function tracks()
    {
        return [
            'bought' => Data::where('title', 'tracks_bought')->first()->content,
            'all' => Data::where('title', 'tracks_free')->first()->content
        ];
    }

    public static function users()
    {
        return User::where('id', '>', '1')->orderBy('tracks_count', 'DESC')->take(10)->get();
    }
}