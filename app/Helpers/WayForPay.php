<?php
/**
 * Created by PhpStorm.
 * User: rakkot
 * Date: 6/8/2019
 * Time: 9:26 PM
 */

namespace App\Helpers;


use App\Track;
use App\User;
use Illuminate\Support\Facades\Auth;

class WayForPay
{
    const API_URL = 'https://api.wayforpay.com/api';

    public $order;
    public $reserve;
    public $orderData;

    public function __construct($reserve = '', $order)
    {
        $this->order = $order;
        $this->reserve = $reserve;
    }

    public function allData()
    {

        $this->orderData();

        $data = $this->dataForSignature();

        $data['merchantSignature'] = hash_hmac("md5", $this->signature($data), env('MERCHANT_SECRETKEY'));

        $data = array_merge($data, [
            'authorizationType' => "SimpleSignature",
            'clientFirstName' => Auth::user()->userName(),
            'clientLastName' => Auth::user()->userLastName(),
            'clientEmail' => Auth::user()->email,
            'clientPhone' => Auth::user()->phone, //"380631234567",
            'language' => "UA"
        ]);

        return $data;
    }

    public function checkStatus()
    {
        $sData = implode(';', [env('MERCHANT_ACCOUNT'), 'DH'.$this->order->id]);

        $signature = hash_hmac("md5", $sData, env('MERCHANT_SECRETKEY'));

        $data = [
            'transactionType' => "CHECK_STATUS",
            'merchantAccount' => env('MERCHANT_ACCOUNT'),
            'orderReference' => 'DH'.$this->order->id,
            'merchantSignature' => $signature,
            'apiVersion' => 1,
        ];

        $result = $this->query($data);

        return $result['transactionStatus'];
    }

    private function orderData()
    {
        $this->orderData['amount'] = 0;
        $this->orderData['pNames'] = [];
        $this->orderData['pPrices'] = [];
        $this->orderData['pCounts'] = [];

        foreach($this->reserve->tracks as $track) {
            $this->orderData['amount'] = $this->orderData['amount'] + $track->amount;
            $productTitle = $track->title ? $track->title . ' #' . $track->id : '#' . $track->id ;

            array_push($this->orderData['pNames'], $productTitle);
            array_push($this->orderData['pPrices'], $track->amount);
            array_push($this->orderData['pCounts'], 1);
        }
    }

    private function signature($data)
    {
        $data['productName'] = implode(';', $data['productName']); //"Процессор Intel Core i5-4670 3.4GHz",
        $data['productPrice'] = implode(';', $data['productPrice']); //1000.00
        $data['productCount'] = implode(';', $data['productCount']);

        return implode(';', $data);
    }

    private function dataForSignature()
    {
        return [
            'merchantAccount' => env('MERCHANT_ACCOUNT'),
            'merchantDomainName' => env('MERCHANT_DOMAIN'),
            'orderReference' => 'DH'.$this->order->id,
            'orderDate' => $this->order->created_at->timestamp,
            'amount' => $this->orderData['amount'],//"1547.36",
            'currency' => "UAH",
            'productName' => $this->orderData['pNames'], //"Процессор Intel Core i5-4670 3.4GHz",
            'productCount' => $this->orderData['pCounts'], //"1",
            'productPrice' => $this->orderData['pPrices'], //1000.00
        ];
    }

    private function query($data)
    {
        $fields = json_encode($data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::API_URL);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json;charset=utf-8'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$fields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        $response = curl_exec($ch);
        curl_close($ch);

        return json_decode($response, true);
    }
}