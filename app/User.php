<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Laravelista\Comments\Commenter;

class User extends Authenticatable
{
    use Notifiable, Commenter;

    const ADMIN_ID = 1;
    const ADMINS = [1, 1];
    const USER_TRACKING_ID = 1;
    const MAP_MODS = ['auto', 'retro', 'night'];

    public $defaultPhoto = '/images/users/no-avatar.gif';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'emailIsPublic', 'phoneIsPublic',
        'trackIsPublic', 'countryCod', 'photo', 'about', 'provider', 'provider_id',
        'map_mod'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function rating()
    {
        $rating = 0;

        if($this->id != 1) {
            $usersByTracks = User::where('id', '>', '1')
                ->orderBy('tracks_count', 'DESC')
                ->where('id', '>=', $this->id)
                ->pluck('id');
            $rating = array_search($this->id, $usersByTracks->toArray()) + 1;
        }

        return $rating;
    }

    public function isOwner()
    {
        return $this->id == Auth::id();
    }

    public function tracks()
    {
        return $this->hasMany('App\Track', 'user_id', 'id');
    }

    public function reserves()
    {
        return $this->hasMany('App\Reserve', 'user_id', 'id');
    }

    public function canAdmin()
    {
        $canAdmin = array_search($this->id, self::ADMINS);
        return $canAdmin !== false;
    }

    public function isTracking()
    {
        return $this->id == self::USER_TRACKING_ID;
    }

    public function getPhoto()
    {
        return $this->photo ? $this->photo : $this->defaultPhoto ;
    }

    public function activeReserve()
    {
        return $this->hasOne('App\Reserve', 'user_id', 'id')
            ->where('updated_at', '>', Carbon::now()->subHours(Reserve::RESERVE_TIME))
            ->where('order_id', null);
    }

    public function reservedTracks()
    {
        if($this->activeReserve) {
            $reserve = $this->activeReserve()->with('tracks')->first();
            return $reserve->tracks;
        }
    }

    public function userName()
    {
        return explode(' ', $this->name)[0];
    }

    public function userLastName()
    {
        $name = explode(' ', $this->name);
        return count($name) > 1 ? $name[1]: $name[0];
    }

    public function getActiveReserve()
    {
        $reserve = $this->activeReserve()->first();
        return $reserve ? $reserve->id : '' ;
    }

    public function updateTracksCount()
    {
        $this->tracks_count = $this->tracks->count();
        $this->save();
    }

    public function messages()
    {
        return $this->hasMany('App\Message');
    }
}
