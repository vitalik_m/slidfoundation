<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShortNew extends Model
{
    use SoftDeletes;

    public $table = 'short_news';

    public $fillable = [ 'content'];
}
