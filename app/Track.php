<?php

namespace App;

use App\Services\TracksByDistances;
use App\Services\TracksByPosition;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Track extends Model
{
    const IMG_PATH = 'images/tracks/';
    const DEFAULT_IMG = self::IMG_PATH . 'default.png';
    const DEFAULT_POSITION = [ 'lat' => 59.91273, 'lon' => 10.74609];

    public $statuses = [
        'pending' => 'pending',
        'checked' => 'checked',
        'deny' => 'deny',
    ];

    protected $fillable = [
        'lon', 'lat', 'title', 'content', 'video', 'amount'
    ];

    public static function countTrackPerQuery()
    {
        return Auth::user() && Auth::user()->isTracking() ? env('TRACKS_PER_QUERY_FOR_ADMIN') : env('TRACKS_PER_QUERY') ;
    }

    public function owner()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function reserve()
    {
        return $this->hasOne('App\Reserve', 'id', 'reserve_id');
    }

    public static function currentPosition()
    {
        $current = self::where('isCurrent', 1)->first();

        if(!$current){
            $current = self::where('id', 1)->first();
        }

        return $current;
    }

    public function sibilings(int $count = 1000)
    {
//        $tracksService = new TracksByDistances($this);
        $tracksService = new TracksByPosition($this);

        $this->checkTracksReserve($tracksService->tracks);

        return $tracksService->tracks;
    }

    public function getImg()
    {
        return $this->img ? $this->img : self::DEFAULT_IMG ;
    }

    public function setLastTrackContent()
    {
        $current = self::currentPosition();

        if($current && $current->id > 1) {
            $nearest = self::where('id', '<', $current->id)->whereNull('order_id')->orderBy('id', 'desc')->first();
            if($nearest->id != $this->id) return;
        }

        if(!empty($nearest)){
            $shortNew = (new ShortNew())->latest()->first();
        }

        if(!empty($nearest) && !empty($shortNew)){
            $this->content = $shortNew->content;
        }
    }

    public function isInPaymentProcess()
    {
        if(($this->reserve && $this->reserve->order) && !$this->order_id){
            return true;
        }
    }

    public static function nearestBy($position)
    {
        $query = 'SELECT id, ( 3959 * acos( cos( radians('.$position['lat'].') ) * cos( radians( lat ) ) * ';
        $query = $query.'cos( radians( lon ) - radians('.$position['lon'].') ) + sin( radians('.$position['lat'].') ) * ';
        $query = $query.'sin( radians( lat ) ) ) ) AS distance FROM tracks HAVING ';
        $query = $query.'distance < 10000 ORDER BY distance LIMIT 0 , 1';

        $nearest = DB::select($query);

        return $nearest[0]->id;
    }

    public static function setCurrent($id)
    {
        $track = self::find($id);
        $track->isCurrent = 1;
        return $track->save();
    }

    public static function unsetOldCurrent($id)
    {
        $oldCurrent = self::currentPosition();
        $oldCurrent->isCurrent = 0;
        return $oldCurrent->save();
    }

    public function isReserved(){
        if($this->reserve){
            if($this->reserve->isActual()){
                return true;
            }else{
                $this->reserve_id = null;
                $this->save();
            }
        }

        return false;
    }

    public function isBought(){
        return !$this->owner->canAdmin();
    }

    private function checkTracksReserve($tracks)
    {
        foreach($tracks as $track){
            if($track->reserve){
                $track->isReserved();
            }
        }
    }
}
