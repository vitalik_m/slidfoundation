$(document).ready(function() {
    $(".js__chatbox-container__close").click(function() {
        $(".js__chatbox-container").fadeOut();
        $(".js__img-container").fadeIn();
    });

    $(document).mouseup(function(e) {
        var block = $(".js__chatbox-container");
        if (!block.is(e.target) && block.has(e.target).length === 0) {
            block.hide();
            $(".js__img-container").fadeIn();
        }
    });

    $(".sl-sidebar-open").click(function() {
        $(".js__chatbox-container").fadeOut();
        $(".js__img-container").fadeIn();
    });
});