$(".image-upload").change(function(input) {
    if (input.target.files && input.target.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#preview-img').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.target.files[0]);
    }
});