(function ($) {
  "use strict";

  $('.scrollbar-inner').scrollbar({
    "autoScrollSize": false,
    "scrolly": $('.external-scroll_y')
  });

  $('.sl-sidebar__scrollbar').scrollbar({
    "autoScrollSize": false,
    "scrolly": $('.sl-sidebar_external-scroll_y')
  });


  let mainWrapper = $('.main-wrapper'),
    sidebar = $('.sl-sidebar');
  $('.sl-sidebar-open').on('click', function () {
    sidebar.toggleClass('active-sidebar');
    mainWrapper.toggleClass('active-wrap')
  });

  /* --------------- Deleting placeholder focus --------------- */
  let inputPlaceholder = $('input, textarea');
  inputPlaceholder.on('focus', function () {
    $(this).data('placeholder', $(this).attr('placeholder'));
    $(this).attr('placeholder', '')
  });

  inputPlaceholder.on('blur', function () {
    $(this).attr('placeholder', $(this).data('placeholder'))
  });
  /* ------------- End Deleting placeholder focus ------------- */

})(jQuery);