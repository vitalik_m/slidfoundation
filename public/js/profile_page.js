$(document).ready(() => {
    // user-name
    $("#js__edit-button__container").click(function () {
        $(this).css("display", "none");
        $(this)
            .closest(".js_form")
            .find(".js_user-name")
            .css("display", "none");
        $(this)
            .closest(".js_form")
            .find(".update-name")
            .css("display", "flex");
    });
    // user-photo
    $("#js__edit-button__container-photo").click(function () {
        $(this).css("display", "none");
        $(this)
            .closest(".js_form")
            .addClass("container-wrap");
        $(this)
            .closest(".js_form")
            .find(".sl-office-page__img")
            .css("display", "none");
        $(this)
            .closest(".js_form")
            .find(".js__update-photo")
            .css("display", "flex")
            .css("margin-bottom", "10px");
    });
    // user-cellphone
    $("#js__edit-button__container-cellphone").click(function () {
        $(this).css("display", "none");
        $(this)
            .parent()
            .find(".py-5__flex")
            .find(".js__cellphone")
            .css("display", "none");
        $(this)
            .parent()
            .find(".py-5__flex")
            .find(".update-phone")
            .addClass("container-wrap");
        $(this)
            .parent()
            .find(".py-5__flex")
            .find(".btn-save")
            .css("margin", "0");
        $(this)
            .parent()
            .find(".py-5__flex")
            .find(".update-phone")
            .css("display", "flex");
    });
    // about
    $("#js__edit-button__container-about").click(function () {
        $(this).css("display", "none");
        $(this)
            .closest(".sl-office-page-col")
            .find(".js__about")
            .css("display", "none");
        $(this)
            .closest(".sl-office-page-col")
            .find(".js__about-form")
            .css("display", "flex")
            .css("flex-direction", "column");
    });
    // card-form
    $(".js__edit-card").click(function () {
        $(this)
            .closest(".js__card-history")
            .find(".card-form")
            .css("display", "flex")
            .css("flex-direction", "column");
    });

    // popup: buy, template - info, owner
    $(".js__popup-buy__close").click(function () {
        $(this)
            .closest(".popup-buy")
            .css("display", "none");
    });
    $(".js__popup-inform").click(function () {
        $(this)
            .closest(".popup-inform")
            .css("display", "none");
    });
    $(".js__popup-owner__close").click(function () {
        $(this)
            .closest(".popup-owner")
            .css("display", "none");
    });

    // удаление элемента и пересчет общей стоимости заказа
    // var cross_collection = $(".js__cart-pos__delete");
    // cross_collection.click(function() {
    //     let track_id = $(this).parent().find('[name=track_id]').attr('value');
    //
    //     cart.unReserve(this, track_id);
    // });
    cart.initItemCloseBtns();

    cart.init();


    $("#cart .button-buy").click(e =>  payment.wayforpayInit(e));
    var wayforpay = new Wayforpay();
});

function showNotif(msgText, msgTitle) {
    if (msgTitle == undefined) {
        $("#js__inform-popup .popup-inform__title").text("");
    } else {
        $("#js__inform-popup .popup-inform__title").text(msgTitle);
    }
    $("#js__inform-popup .popup-inform__description").text(msgText);
    // $("#js__inform-popup").css("display", "block");
    $("#js__inform-popup").show('fast')

    setTimeout(() => {
        $("#js__inform-popup").hide('fast')
    }, 2000);
}

var cart = {
    init: () => {
        $(".button-buy__book").click(e => cart.addTrack(e));
        $(".button-buy__later").click(e =>  cart.addTrack(e, true));

        $("#cart .sl-form-login__close").click(() => cart.showCartClosingNotif());

        cart.calcutaleTotalSum();
    },
    addTrackCallback : (e, result, cartAction) => {
        if (result.status == "ok") {
            let track_id = slid_map.getTrackIdFromEvent(e);

            cart.addItemCart(e.target);
            cart.toggleViewBuyItemBtns();
            cart.toggleViewDescTexts($(e.target).parent().parent());

            cart.initItemCloseBtns();
            $('.items-cart-form [name=reserve_id]').attr('value', result.reserv_id);
            $('#del-cart-item [name=reserve_id]').attr('value', result.reserv_id);
            cart.calcutaleTotalSum();
            cart.viewCartIcon('open');

            slid_map.hideTrackPopup(track_id);
            slid_map.changeTrackIcon(track_id, 'reserved');

            if (cartAction != undefined) {
                let modal_cart = $("[data-remodal-id=cart]").remodal();
                modal_cart.open();
            } else {
                showNotif(result.msg, "Дякуємо!");
                // showNotif("товар добавлен в корзину", "Дякуємо!")
            }
        } else {
            showNotif(result.msg, "Помилка!");
            // showNotif("ветка ошибки после запроса о статусе брони", "Помилка!");
        }
    },
    addTrack: (e, cartAction) => {
        e.preventDefault();
        if (!cart.isAuth()) return;

        cart.reserve(e, cart.addTrackCallback, cartAction);
    },
    addItemCart: function (choosen_popup) {
        let choosen_popup_title = $(choosen_popup)
            .closest(".gm-style-iw-d")
            .find(".popup-buy__title")
            .text();
        let choosen_popup_price = $(choosen_popup)
            .closest(".gm-style-iw-d")
            .find(".popup-buy__price")
            .text()
            .replace(/[^0-9.]/gi, "");
        let price = parseInt(choosen_popup_price, 10);
        let track_id = $(choosen_popup).parent().find('[name=track]').attr('value');

        $(".sl-form-cart form .d-flex.flex-column.pt-30").append(`
            <div class="d-flex flex-wrap cart-pos" id="cart-track-">
                <input type="hidden" name="track_id" value="${track_id}">
                <p class="cart-pos__title">${choosen_popup_title}</p>
                <p class="cart-pos__price"><span>${price}</span> грн</p>
                <span class="chatbox-container__close cart-pos__delete js__cart-pos__delete"></span>
            </div>`);
    },
    initItemCloseBtns: function () {
        $(".js__cart-pos__delete").off('click');
        $(".js__cart-pos__delete").on('click', function () {
            let track_id = $(this).parent().find('[name=track_id]').attr('value');

            cart.unReserve(this, track_id);

        });
    },
    calcutaleTotalSum : () => {
        var sum_block = $(".cart-pos .cart-pos__price span");
        var sum_block_value = 0;
        for (var i = 0; i < sum_block.length; i++) {
            // console.log(parseFloat(sum_block[i].textContent));
            sum_block_value += parseFloat(sum_block[i].textContent);
        }
        $(".cart-total .cart-total__price span").html(sum_block_value);
    },
    reserve: (e, callback, cartAction) => {
        let form = e.target.parentElement;

        let url = $(form).attr("action");
        let data = $(form).serialize();

        $.post(url, data, function(result) {
            callback(e, result, cartAction);
        }).fail(function(result) {
            let msg = result.result.responseJSON.message ? result.result.responseJSON.message : 'Щось пішло не так....' ;
            callback(e, {status:'error', msg : msg});
        });
    },
    unReserve: (elem, track_id) => {
        let form = $('#del-cart-item').first();
        $('#del-cart-item [name=track]').attr('value', track_id);
        let data = $(form).serialize();
        let url = $(form).attr("action");

        $.post(url, data, (result) => {
            if(result.status == 'ok') {
                $(elem).closest(".cart-pos").remove();

                cart.popupStateToFree(track_id);

                slid_map.changeTrackIcon(track_id, 'free');

                cart.calcutaleTotalSum();
                if (cart.isEmpty()) {
                    cart.viewPopup('close');
                    cart.viewCartIcon('close');
                }
            }else {
                showNotif('Щось пішло не так :( ', 'Помилка');
            }
        }).fail(function(result) {
            showNotif('Щось пішло не так :( ', 'Помилка')
        });
    },
    isEmpty: () => {
        if ($("form .cart-pos").children().length == 0) {
            return true;
        }else{
            return false;
        }
    },
    viewCartIcon: (action) => {
        let btn = $('#open_cart_btn');

        if(action === 'open'){
            btn.show();
        }else if(action === 'close'){
            btn.hide();
        }
    },
    viewPopup: (action) => {
        let modal_cart = $("[data-remodal-id=cart]").remodal();

        if(action === 'open'){
            modal_cart.open();
        }else if(action === 'close'){
            modal_cart.close();
        }
    },
    toggleViewBuyItemBtns: (elemTrackPopupBatns) => {
        $(elemTrackPopupBatns).find('.buy-form ').hide();
        $(elemTrackPopupBatns).find('.reserved-block').show();
    },
    toggleViewDescTexts: (elemTrackPopupBatns) => {
        $(elemTrackPopupBatns).parent().find('.text-reserved ').toggle();
        $(elemTrackPopupBatns).parent().find('.text-free').toggle();
    },
    isAuth: function() {
        let isUserLogin = $(".sl-adminbar__personal-area").length;
        if (isUserLogin !== 0) {
            return true;
        } else {
            let modal_login = $("[data-remodal-id=login]").remodal();
            modal_login.open();
            return false;
        }
    },
    popupStateToFree: (track_id) =>{
        let elemTrackPopupBatns = $('#trackPopupBtns-'+track_id);

        $(elemTrackPopupBatns).find('.buy-form ').show();
        $(elemTrackPopupBatns).find('.reserved-block').hide();
        cart.toggleViewDescTexts(elemTrackPopupBatns);
    },
    popupStateToReserved: (track_id) => {
        let elemTrackPopupBatns = $('#trackPopupBtns-'+track_id);

        $(elemTrackPopupBatns).find('.buy-form ').hide();
        $(elemTrackPopupBatns).find('.reserved-block').show();
    },
    isInCart: (track_id) => {
        return $('.items-cart-form [name=track_id][value='+track_id+']').length > 0;
    },
    checkTrackStatus: (track_id) => {
        if(cart.isInCart(track_id)){
            cart.popupStateToReserved(track_id);
        }else{
            cart.popupStateToFree(track_id);
        }
    },
    showCartClosingNotif : () => {
        let msg = texts.cart_closing_notif ? texts.cart_closing_notif : 'gso!';
        showNotif(msg, "");
    }
};

var wayforpay = new Wayforpay();

var payment = {
    wayforpayInit: function (e) {
        e.preventDefault();

        let form = $(e.target).closest('.items-cart-form');
        let url = $(form).attr("action");
        let data = $(form).serialize();

        // wayforpay.closeit();
        $.post(url, data, function(result) {
            payment.wayforpayRun(result);

            let modal_cart = $("[data-remodal-id=cart]").remodal();
            modal_cart.close();
        }).fail(function(result) {
            showNotif(result.message, "Помилка!");
        });
    },
    approved: function (response) {
        let form = $('.items-cart-form').first();
        let url = routes.orderApprove;
        let data = $(form).serialize();

        $.post(url, data, function(result) {
            showNotif(result.message, "Дякуемо!");
        }).fail(function(result) {
            // showNotif(result.message, "Помилка!");
        });
    },
    declined: function (response) {
        let form = $('.items-cart-form').first();
        let url = routes.orderDecline;
        let data = $(form).serialize();

        $.post(url, data, function(result) {
        }).fail(function(result) {
        });
    },
    processing: function (response) {
        let form = $('.items-cart-form').first();
        let url = routes.orderApprove;
        let data = $(form).serialize();

        $.post(url, data, function(result) {
            // showNotif(result.message, "Дякуемо!");
        }).fail(function(result) {
            // showNotif(result.message, "Помилка!");
        });
    },
    wayforpayRun : (result) => {
        wayforpay.run(
            result,
            function (response) {
                payment.approved()
            },
            function (response) {
                payment.declined()
            },
            function (response) {
                payment.processing()
            }
        );
    }
};