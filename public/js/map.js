var slid_map = {
    init : (mode) => {
        if(map_data.isAdmin){
            slid_map.setRealCurrentPosition();

            slid_map.initMap(mode);
        }else{
            slid_map.initMap(mode);
        }
    },
    createMap: function () {
        map = new google.maps.Map(document.getElementById("makemap_canvas"), slid_map.options());
        map.scrollwheel = true;
        map.setOptions({mapTypeControl: true});
        slid_map.map = map;

        map_updater.bindEventsMap();
    },
    initMap : (mode) => {
        slid_map.setMapMode();
        slid_map.markers = [];

        slid_map.createMap();

        // slid_map.setMarkersOpacity();
        slid_map.addMarkers();
        slid_map.setValeraPosition();

        // slid_map.bindEventsMap();
    },
    setMapMode : () => {
        let now = new Date().getHours();

        if( (map_data.userMapTheme === 'auto' && (now > 5 && now < 22)) || map_data.userMapTheme === 'retro'){
            slid_map.mapMode = 'retro';
        }else if(map_data.userMapTheme === 'night' || (now < 5 || now >= 22)){
            slid_map.mapMode = 'night';
        }
    },
    addMarkers : () => {
        for (var element in map_data.tracks) {

            let icon = slid_map.getTrackIcon(map_data.tracks[element]);

            let marker = new google.maps.Marker({
                position: {lat : parseFloat(map_data.tracks[element].lat), lng: parseFloat(map_data.tracks[element].lon)},
                map: map,
                title: slid_map.getTrackTitle(map_data.tracks[element]),
                icon: icon
            });

            marker.infowindow = slid_map.getTrackPopup(map_data.tracks[element]);
            marker.infowindow.addListener('domready', () => cart.init());

            marker.addListener('click', (e) => { slid_map.showTrackPopup( e, marker.infowindow, marker);});

            slid_map.markers[ map_data.tracks[element].id ] = marker;

            slid_map.setMapOnTrack(map, map_data.tracks[element].id);
        }
    },
    showTrackPopup : (e, infowindow, marker) => {
        let trackId = $(infowindow.content).first().data('track');

        if(trackId) {
            $.get( map_data.trackPopupRoute + '/' + trackId, '', (result) => {
                infowindow.setContent(result.data);
                infowindow.open(map, marker);
                // $(infowindow.content).append(result.data)[0];
            }).fail(function (result) {
                return result; ///TODO render msg error
            });
        }else{
            infowindow.open(map, marker);

            let track_id = $(infowindow.content).find('[name=track]').attr('value');
            cart.checkTrackStatus(track_id);
        }
    },
    setMapOnAll : (map) => {
        map = !map ? map : slid_map.map;

        slid_map.markers.forEach((marker, id) => {
            slid_map.markers[id].setMap(map);
        });
    },
    setMapOnTrack : (map, track_id) => {
        map = map ? map : slid_map.map;

        slid_map.markers.forEach((marker, id) => {
            slid_map.markers[track_id].setMap(map);
        });
    },
    clearMarkers : () => {
        slid_map.setMapOnAll(null);
    },
    deleteMarkers : () => {
        slid_map.clearMarkers();
        slid_map.markers = [];
    },
    setValeraPosition : () => {
        if(slid_map.valeraMarker) slid_map.valeraMarker.setMap(null);

        slid_map.valeraMarker =  new google.maps.Marker({
            position: {lat : map_data.currentPosition.lat , lng: map_data.currentPosition.lon},
            map: map,
            title: 'Валера зараз тут!',
            icon: 'images/markers/valera.png'
        });

        slid_map.bindCurrentPositionBtn();
    },
    setValeraRealPosition : () => {
        slid_map.valeraRealMarker =  new google.maps.Marker({
            position: {lat : slid_map.realCurrentPosition.lat , lng: slid_map.realCurrentPosition.lon},
            map: map,
            title: 'Валера насправді тут!',
            icon: 'images/markers/valera_real.png'
        });
    },
    setRealCurrentPosition : () => {
        navigator.geolocation.getCurrentPosition((position) => {

            slid_map.realCurrentPosition = {
                lat: position.coords.latitude,
                lon: position.coords.longitude
            };

            slid_map.setMapData();
        });
    },
    setMapData : () => {
        slid_map.setValeraRealPosition();

        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type:'POST',
            url: map_data.updateCurrentPositionRoute,
            data: {position : slid_map.realCurrentPosition},
            success:function(data){
                if(data.status === 'ok'){
                    window.location.replace(map_data.homePageRoute);
                }
            }
        });
    },
    options : () => {
        return {
            zoom: 17,
            center: new google.maps.LatLng(map_data.centerPosition.lat, map_data.centerPosition.lon),
            navigationControlOptions: {
                style: google.maps.NavigationControlStyle.SMALL
            },
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            maxZoom:20,
            styles: slid_map.mapTheme()
        }
    },
    bindCurrentPositionBtn : () => {
        $('.current-info .place a').click((e) => {
            e.preventDefault();
            map.setCenter( slid_map.valeraMarker.getPosition());
        });
    },
    getTrackTitle: function (element) {
        let title = element.title ? element.title : '';

        return title +' #'+element.id;
    },
    getTrackPopup: function (element) {
        let contentString = '<div class="marker_popup-content marker" ' +
            'id="marker-'+ element.id +'" data-track='+ element.id +'></div>';

        return new google.maps.InfoWindow({
            content: contentString
        });
    },
    getTrackIcon: function (element) {
        // let opacity = slid_map.getTrackMarkerOpacity(element);
        let opacity = '';
        let icon;

        if(element.user_id > 1){
            icon = {url: slid_map.iconBoughtTrack(opacity), scaledSize: new google.maps.Size(20, 20)};
        }else if(element.reserve_id){
            icon = {url: slid_map.iconReservedTrack(opacity), scaledSize : new google.maps.Size(20, 20)};
        }else {
            icon = {url: slid_map.iconFreeTrack(opacity), scaledSize : new google.maps.Size(20, 20)};
        }

        icon.anchor = new google.maps.Point(10, 10);

        return icon;
    },
    changeTrackIcon: (track_id, newIcon) => {
        let urlIcon;

        if(newIcon === 'reserved'){
            urlIcon = slid_map.iconReservedTrack('');
        }else if(newIcon === 'free'){
            urlIcon = slid_map.iconFreeTrack('');
        }

        slid_map.markers[track_id].icon = {
            url: urlIcon,
            scaledSize : new google.maps.Size(20, 20),
            anchor : new google.maps.Point(10, 10)
        };

        slid_map.markers[track_id].setMap(null);
        slid_map.markers[track_id].setMap(slid_map.map);
    },
    getTrackMarkerOpacity : (element) => {
        if(element.id < map_data.currentPosition.id){
            for (var i = 0; i <= slid_map.arrOpacity.toCurrent.length; i++) {
                if(element.id < slid_map.arrOpacity.toCurrent[i]){
                    return slid_map.arrOpacity.opacityProcent[i];
                    break;
                }
            }
            // slid_map.arrOpacity.toCurrent.forEach((id, index) => {
            //     if(element.id < id){
            //         return index;
            //         b
            //     }
            // });
        }else {
            for (var i = 0; i <= slid_map.arrOpacity.fromCurrent.length; i++) {
                if(element.id > slid_map.arrOpacity.fromCurrent[i]){
                    return slid_map.arrOpacity.opacityProcent[i];
                    break;
                }
            }
            // slid_map.arrOpacity.fromCurrent.findIndex((id, index) => {
            //     if(element.id > id){
            //         return slid_map.arrOpacity.procent[index];
            //     }
            // });
        }
    },
    setMarkersOpacity: function () {
        let countKiesToCurrent = map_data.currentPosition.id - Object.keys(map_data.tracks)[0];
        let countKiesFromCurrent = Object.keys(map_data.tracks).length - countKiesToCurrent;

        slid_map.arrOpacity.tracksProcent.forEach((procent) => {
            let countKiesForPlusStart = Math.round(countKiesToCurrent / 100 * procent);
            let key = Object.keys(map_data.tracks)[countKiesForPlusStart];

            slid_map.arrOpacity.toCurrent.push( key );
        });

        slid_map.arrOpacity.tracksProcent.forEach((procent) => {
            let countKiesForMinusFromEnd = Math.round(countKiesFromCurrent / 100 * procent);
            let indexLastKey = Object.keys(map_data.tracks).length-1;

            let key = Object.keys(map_data.tracks)[indexLastKey] - countKiesForMinusFromEnd;

            slid_map.arrOpacity.fromCurrent.push( key );
        });
    },
    iconFreeTrack : (opacity) => {
        let opacitySufix = opacity ? '_'+opacity : '';

        if( slid_map.mapMode === 'retro'){
            return "images/markers/for_light"+opacitySufix+".png";
        }else if(slid_map.mapMode === 'night'){
            return "images/markers/for_dark"+opacitySufix+".png";
        }
    },
    iconBoughtTrack : () => {
        return "images/markers/for_bought.png";
    },
    iconReservedTrack : () => {
        return "images/markers/for_reserved.png";
    },
    mapTheme : () => {
        if( slid_map.mapMode === 'retro'){
            return [];
        }else if(slid_map.mapMode === 'night'){
            return map_data.nightStyleMap;
        }
    },
    getTrackIdFromEvent: function (e) {
        return e.target.parentElement.querySelector('[name=track]').value;
    },
    hideTrackPopup: (track_id) => {
        slid_map.markers[track_id].infowindow.close()
    },
    arrOpacity : {
        opacityProcent : ['30', '50', '70', '90', ''],
        tracksProcent : ['5', '15', '25', '35', '100'],
        toCurrent : [],
        fromCurrent : []
    }
};

var map_updater = {
    tracksQuery: (mapData) => {
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type: 'GET',
            url: map_data.updateTracksByBounds,
            data: {cordinates: mapData.cordinates, zoom: mapData.zoom},
            beforeSend: function () {
                $('#ico_tracks_loader').addClass('loader').text('Сліди оновлюются').fadeIn('fast');
            },
            success: function (data) {
                $('#ico_tracks_loader').text('Завантажено ' + Object.keys(data.tracks).length + ' слідів');

                map_updater.updateMap(data);
            },
            complete: function () {
                map_updater.inProcess = false;
            },
            error: function (data) {
                $('#ico_tracks_loader').text('Помилка завантаження слідів.').removeClass('loader');
            }
        });
    },
    setCurrentPosition: function (current_position) {
        map_data.currentPosition = {
            id : current_position.id,
            lat : parseFloat(current_position.lat),
            lon : parseFloat(current_position.lon)
        };
    },
    updateMap: (data) => {
        slid_map.clearMarkers();

        map_updater.setCurrentPosition(data.current_position);
        map_data.tracks = data.tracks;

        slid_map.addMarkers();
        slid_map.setValeraPosition();

        $('#ico_tracks_loader').fadeOut('slow');

        if(map_updater.lastTracksQueryData) {
            map_updater.tracksQuery(map_updater.lastTracksQueryData);
            map_updater.lastTracksQueryData = '';
        }
    },
    bindEventsMap: function () {
        slid_map.map.addListener('dragend', ()=>{ map_updater.beforeUpdatingMap() });
        slid_map.map.addListener('zoom_changed', ()=>{ map_updater.beforeUpdatingMap() });
    },
    isCordinatesAvailable: function () {
        return slid_map.map.getBounds()
    },
    beforeUpdatingMap: () => {
        if (!map_updater.inProcess && map_updater.isCordinatesAvailable()) {
            map_updater.inProcess = true;

            map_updater.tracksQueryData = map_updater.getCurrentMapData();

            map_updater.tracksQuery(map_updater.tracksQueryData);
        }else{
            map_updater.lastTracksQueryData = map_updater.getCurrentMapData();
        }
    },
    getCurrentMapData: function () {
        return {
            zoom: slid_map.map.getZoom(),
            cordinates: [
                {'lat': slid_map.map.getBounds().na.j, 'lon': slid_map.map.getBounds().ga.j},
                {'lat': slid_map.map.getBounds().na.l, 'lon': slid_map.map.getBounds().ga.l}
            ]
        };
    },
}